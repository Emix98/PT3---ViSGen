package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.visgen.model.Loadable;

/** Repr�sente une pi�ce ou un couloir */
public abstract class MapArea extends Selectable implements Loadable {
	/** Hauteur de la pi�ce ou du couloir (fix�e � trois m�tres) */
	private final static double HEIGHT = 3;
	/** Valeur d�finissant une ouverture ne menant � rien */
	protected final static int DEAD_END = -1;

	/** Identifiant de la salle */
	protected int id;
	/** Forme de la pi�ce */
	protected List<Point> summits;
	/** Liste des ouvertures de la pi�ce */
	protected List<Opening> openings;
	/** Nom de la pi�ce */
	protected String name;



	// CONSTRUCTEURS
	/**
	 * @param shape Polygone qui compose la forme de la pi�ce
	 * @param openings Ouvertures de la pi�ce
	 * @param windows 
	 */
	public MapArea(int id, List<Point> shape, List<Opening> openings) {
		this.id = id;
		this.summits = shape;
		this.openings = openings;
		
	}

	/** Cr�e une pi�ce en fournissant son id
	 * @param id Identifiant de la pi�ce
	 */
	public MapArea(int id) {
		this(id, new ArrayList<Point>(), new ArrayList<Opening>());
	}



	// METHODES
	
	/** Compte le nombre de portes (sert � d�terminer l'ID d'une porte)
	 * @return int Nombre de portes
	 */
	public int getDoorNumber() {
		int number = 0;
		for(Opening o : openings) {
			if(o instanceof Door) number++;
		}
		
		return number;
	}
	
	/** Compte le nombre de fen�tres (sert � d�terminer l'ID d'une fen�tre)
	 * @return int Nombre de fen�tres
	 */
	public int getWindowNumber() {
		int number = 0;
		for(Opening o : openings) {
			if(o instanceof Window) number++;
		}
		
		return number;
	}

	// GETTERS ET SETTERS
	/**
	 * Retourne l'ID de la pi�ce
	 * @return ID de la pi�ce
	 */
	public int getId() {
		return id;
	}

	/**
	 * Retourne le nom de la pi�ce
	 * @return Nom de la salle
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * D�finit le nom de la pi�ce
	 * @param name Nouveau nom de la salle
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne le polygone donnant la forme de la pi�ce
	 * @return Polygone de la forme de la pi�ce
	 */
	public List<Point> getSummits() {
		return summits;
	}

	/**
	 * Retourne la liste des ouvertures de la pi�ce
	 * @return liste des ouvertures de la pi�ce
	 */
	public List<Opening> getOpeningList() {
		return openings;
	}

	/**
	 * Retourne la liste des murs de la MapArea. Utile uniquement si la MapArea est une instance de Room.
	 * @return
	 */
	public abstract List<Wall> getWalls();
	
	/**
	 * Permet d'ajouter un sommet � la pi�ce ou au couloir
	 * @param summit Sommet � ajouter
	 */
	public abstract void addSummit(Point summit);
	
	@Override
	public void save(DataOutputStream data) throws IOException {
		// Ecrit la taille du nom (en caract�res)
		data.writeInt(name.length());
		
		// Ecrit le nom
		data.writeChars(name);

		// Ecrit l'ID
		data.writeInt(id);

		// On sauvegarde le nombre des sommets et les sommets du couloir
		data.writeInt(summits.size());
		for(int i = 0; i < summits.size(); i++) summits.get(i).save(data);

		// On sauvegarde le nombre d'ouvertures et les ouvertures du couloir
		data.writeInt(openings.size());
		for(int i = 0; i < openings.size(); i++) {
			Opening opening = openings.get(i);
			// Porte = 0, fen�tre = 1
			if(opening instanceof Door) data.writeInt(0);
			else if(opening instanceof Window) data.writeInt(1);
			opening.save(data);
		}
	}

	@Override
	public void load(DataInputStream data) throws IOException {
		// Remise � z�ro des tableaux et des attributs
		summits = new ArrayList<Point>();
		openings = new ArrayList<Opening>();
		name = "";
		
		// Charge la taille du nom
		int size = data.readInt();
		
		// Charge le nom
		for(int i = 0; i < size; i++) {
			name += data.readChar();
		}

		// Charge l'ID
		id = data.readInt();

		// Charge le nombre de sommets
		int summitNbr = data.readInt();
		
		// On charge les sommets
		for(int i = 0; i < summitNbr; i++) {
			summits.add(new Point());
			summits.get(i).load(data);
		}

		// Charge le nombre d'ouvertures
		int openingNbr = data.readInt();
		System.out.println("Nombre d'ouvertures: " + openingNbr);
		
		// On charge les ouvertures
		for(int i = 0; i < openingNbr; i++) {
			// Porte 0, fen�tre 1
			int type = data.readInt();

			if(type == 0) openings.add(new Door());
			else openings.add(new Window());
			
			openings.get(i).load(data);
		}
	}
}
