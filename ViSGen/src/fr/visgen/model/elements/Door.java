package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/** Repr�sente une porte */
public class Door extends Opening {
	/** Type de porte (entr�e ?) */
	private boolean isEntrance;
	/** Etat de la porte (ouverte / ferm�e) */
	private boolean isOpen = true;
	/** Etat de la porte (ouverte / bloqu�e) */
	private boolean isBlocked = false;
	
	/** Construit une porte
	 * @param x Coordon�e x de la porte
	 * @param y Coordon�e y de la porte
	 */
	public Door(int id, double x, double y, int linkedRoom) {
		super(id, x, y, linkedRoom);
		width = 1;
		height = 2;
	}
	
	/** Construit une porte par d�faut */
	public Door() {
		this(0, 0, 0, MapArea.DEAD_END);
	}
	
	/** Permet d'ouvrir ou de fermer la porte
	 * 	@param state Etat de la porte (true si ouverte)
	 *  */
	public void setOpen(boolean state) {
		if(isBlocked != true) isOpen = state;
	}
	
	/** Permet de changer l'�tat de la porte (ouverte/bloqu�e)
	 *  @param state Porte bloqu�e ?
	 */
	public void setBlocked(boolean state) {
		isBlocked = state;
		if(state == true) isOpen = false; // On ferme la porte si on veut la bloquer
	}
	
	/** V�rifie si la porte est ferm�e 
	 * @return isOpen true si la porte est ouverte
	 * */
	public boolean isOpen() {
		return isOpen;
	}
	
	/** V�rifie si la porte est bloqu�e 
	 * @return isLocked true sur la porte est bloqu�e
	 * */
	public boolean isBlocked() {
		return isBlocked;
	}
}
