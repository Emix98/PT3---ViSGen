package fr.visgen.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.lang.model.element.Element;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

import fr.visgen.model.elements.MapArea;
import fr.visgen.model.elements.Opening;
import fr.visgen.model.elements.Room;
import fr.visgen.model.elements.Selectable;

/** Mod�le de l'explorateur */
public class ExplorerModel extends Observable {
	
	/** Contient les �l�ments de la carte actuelle */
	private GameMap map;
	/** Pi�ce ou couloir actuel */
	private MapArea currentArea;
	
	// CONSTRUCTEUR(S)
	
	
	// GETTERS / SETTERS
	
	
	  /** Controle de mouvement d'observateur */
	  public final static int TURN_RIGHT = 1;
	  public final static int TURN_LEFT = 2;
	  public final static int NO_RIGHT_LEFT_TURN = 3;
	  public final static int TURN_UP = 4;
	  public final static int TURN_DOWN = 5;
	  public final static int NO_UP_DOWN_TURN = 6;
	  public final static int GO_FRONTWARDS = 7;
	  public final static int GO_BACKWARDS = 8;
	  public final static int NO_FRONT_BACK_GO = 9;
	  public final static int GO_RIGHTWARDS = 10;
	  public final static int GO_LEFTWARDS = 11;
	  public final static int NO_RIGHT_LEFT_GO = 12;
	  public final static int GO_UPWARDS = 13;
	  public final static int GO_DOWNWARDS = 14;
	  public final static int NO_UP_DOWN_GO = 15;

	  /** Projection en perspective (si il est active)*/
	  private boolean perspectiveOn = true;

	  /** Modification de la projection a traiter */
	  private boolean projectionChanged = false;
	  private int azimuthMobility = 0;
	  private int heightMobility = 0;
	  private int frontMobility = 0;
	  private int rightMobility = 0;
	  private int upMobility = 0;

	  /** Angle de perspective (utilise en mode perspective) */
	  private static final double DEFAULT_PERSPECTIVE_ANGLE = 60.0;
	  private static final double PERSPECTIVE_INC = 1.0;
	  private static final double PERSPECTIVE_MIN = 1.0;
	  private static final double PERSPECTIVE_MAX = 150.0;
	  private double perspectiveAngle = DEFAULT_PERSPECTIVE_ANGLE;

	  /** Angle azimut (site) (utilise en mode perspective) */
	  private static final float DEFAULT_AZIMUTH_ANGLE = 0.0f;
	  private static final float AZIMUTH_ANGLE_INC = 1.0f;
	  private float azimuthAngle = DEFAULT_AZIMUTH_ANGLE;

	  /** Angle d'hauteur (elevation) (utilise en mode perspective) */
	  private static final float DEFAULT_HEIGHT_ANGLE = -10.0f;
	  private static final float HEIGHT_ANGLE_INC = 1.0f;
	  private float heightAngle = DEFAULT_HEIGHT_ANGLE;

	  /** Centre de projection (utilise en mode perspective) */
	  private float[] defaultCenter = {0.0f, 0.0f, 0.0f};
	  private float[] center = {0.0f, 0.0f, 0.0f};
	  private static final float TRANSLATION_INC = 0.1f;
	  private boolean inCorr = false;
	 
	  private float centerXmin = -1.0f;
	  private float centerXmax = 1.0f;
	  private float centerYmin = -1.0f;
	  private float centerYmax = 1.0f;
	  private float centerZmin = -1.0f;
	  private float centerZmax = 1.0f;
	  
	  /** Taille de la fenetre */
	  private int vpWidth = 100, vpHeight = 100;

	  /** Taille de la scene affichee (utilisee en mode orthographique) */
	  private double defaultSceneSize = 1.0f;
	  private double sceneSize = defaultSceneSize;
	  private static final double SCENE_SIZE_INC = 1.01;
	  private static final double SCENE_SIZE_MIN_RATIO = 0.1;
	  private static final double SCENE_SIZE_MAX_RATIO = 1.5;
	  private double zMin = 0.0f;
	  private double zMax = 1.0f;
	  private Box obby = new Box (0.1f, 0.1f, 0.01f);

	  /** Point de focalisation (Utilise en mode orthographique) */
	  private float[] defaultFocus = {0.0f, 0.0f};
	  private float[] focus = {0.0f, 0.0f};
	  private static final float FOCUS_INC = 0.05f;

	  /** Acces au fonctions GLU */
	  private GLU glu = new GLU ();

	  /** Reinitialise la matrice de projection en fonction de la nouvel fenetre et de la valeurs d'angle de projection
	    * @param gl OpenGL2 context.
	    * @param width viewport width.
	    * @param height viewport height.
	    */ 
	  public void setProjection (GL2 gl, int width, int height)
	  {
	    vpWidth = width;
	    vpHeight = height;
	    setProjection (gl);
	  }

	  /** Reinitialise la matrice de projection en fonction du nouvel angle de projection
	    * @param gl OpenGL2 context.
	    */ 
	  public void setProjection (GL2 gl)
	  {
	    gl.glMatrixMode (GL2.GL_PROJECTION);
	    gl.glLoadIdentity ();
	    if (perspectiveOn)
	      glu.gluPerspective (perspectiveAngle, (double) vpWidth / vpHeight,
	                          0.01, 30.0);
	    else
	      gl.glOrtho (- sceneSize * vpWidth / (2 * vpHeight),
	                  sceneSize * vpWidth / (2 * vpHeight),
	                  - sceneSize / 2, sceneSize / 2, zMin, zMax);
	    gl.glMatrixMode (GL2.GL_MODELVIEW);
	  }

	  /** Definit les matrices de projection et de visualisation avant d'afficher une scene
	    * @param gl GL2 context.
	    */
	  public void apply (GL2 gl)
	  {
	    // Restaure la projection actuelle
	    if (projectionChanged)
	    {
	      setProjection (gl);
	      projectionChanged = false;
	    }

	    // Remet � jour la vue
	    gl.glLoadIdentity ();

	    boolean moved = false;

	    // met a jour les angles azimut et hauteur
	    if (azimuthMobility != 0)
	    {
	      azimuthAngle += AZIMUTH_ANGLE_INC * azimuthMobility;
	      if (azimuthAngle > 180.0f) azimuthAngle -= 360.0f;
	      else if (azimuthAngle < -180.0f) azimuthAngle += 360.0f;
	    }
	    if (heightMobility != 0)
	    {
	      heightAngle += HEIGHT_ANGLE_INC * heightMobility;
	      if (heightAngle > 90.0f) heightAngle = 90.0f;
	      else if (heightAngle < -90.0f) heightAngle = -90.0f;
	    }

	    // Traiter les mouvements arri�re / avant
	    if (frontMobility != 0)
	    {
	      float cb = TRANSLATION_INC * frontMobility
	                   * (float) Math.cos (heightAngle * Math.PI / 180);
	      center[0] -= cb * (float) Math.cos (azimuthAngle * Math.PI / 180);
	      center[1] -= cb * (float) Math.sin (azimuthAngle * Math.PI / 180); 
	      center[2] += TRANSLATION_INC * frontMobility
	                   * (float) Math.sin (heightAngle * Math.PI / 180);
	      moved = true;
	    }

	    // Traiter les mouvements droite / gauche
	   if (rightMobility != 0)
	    {
	      center[0] -= TRANSLATION_INC * rightMobility
	                   * (float) Math.sin (azimuthAngle * Math.PI / 180);
	      center[1] += TRANSLATION_INC * rightMobility
	                   * (float) Math.cos (azimuthAngle * Math.PI / 180);
	      moved = true;
	    }
	 
	    //  Traiter les mouvements haut / bas
	    if (upMobility != 0)
	    {
	      float sa = TRANSLATION_INC * upMobility
	                 * (float) Math.sin (heightAngle * Math.PI / 180);
	      center[0] += sa * (float) Math.cos (azimuthAngle * Math.PI / 180);
	      center[1] += sa * (float) Math.sin (azimuthAngle * Math.PI / 180); 
	      center[2] += TRANSLATION_INC * upMobility
	                   * (float) Math.cos (heightAngle * Math.PI / 180);
	      moved = true;
	    }

	    // Applique le centre de projection dans le volume accessible
	    if (moved)
	    {
	      // Hauteur
	      if (center[2] > centerZmax) center[2] = centerZmax;
	      else if (center[2] < centerZmin) center[2] = centerZmin;

	      
	      // Reste dans la pi�ce
	        if (center[0] > centerXmax) center[0] = centerXmax;
	        else if (center[0] < centerXmin) center[0] = centerXmin;
	        if (center[1] > centerYmax) center[1] = centerYmax;
	        else if (center[1] < centerYmin) center[1] = centerYmin;
	      }
	    
	    
	    if (perspectiveOn)
	    {
	    // Vient au point d'observation,
	    // regarde ensuite la direction -X, avec la direction Y sur la droite,
	    // applique enfin les angles d'azimut et d'�l�vation.
	      gl.glRotatef (- heightAngle, 1.0f, 0.0f, 0.0f);
	      gl.glRotatef (- azimuthAngle, 0.0f, 1.0f, 0.0f);
	      gl.glRotatef (- 90.0f, 1.0f, 0.0f, 0.0f);
	      gl.glRotatef (- 90.0f, 0.0f, 0.0f, 1.0f);
	      gl.glTranslatef (- center[0], - center[1], - center[2]);
	    }

	    else // Projection orthographic
	    {
	      // Arrive � la mise au point et regard vers le bas
	      gl.glTranslatef (focus[1], focus[0], 0.0f);
	      gl.glRotatef (-90.0f, 0.0f, 0.0f, 1.0f);
	    }

	    // Dessine l'observeur comme une boite
	    gl.glPushMatrix ();
	      gl.glTranslatef (center[0], center[1], center[2]);
	      obby.draw (gl);
	    gl.glPopMatrix ();
	  }

	  /** Bascule le type de projection (perspective / orthographique).
	    */ 
	  public void toggleProjection ()
	  {
	    perspectiveOn = ! perspectiveOn;
	    projectionChanged = true; // � traiter au prochain affichage
	  }

	  /** Retrecit la zone de vision.
	    */
	  public void zoomIn ()
	  {
	    if (perspectiveOn)
	    {
	      perspectiveAngle -= PERSPECTIVE_INC;
	      if (perspectiveAngle < PERSPECTIVE_MIN)
	        perspectiveAngle = PERSPECTIVE_MIN;
	    }
	    projectionChanged = true; // � traiter au prochain affichage
	  }

	  /** Elargie la zone de vision.
	    */
	  public void zoomOut ()
	  {
	    if (perspectiveOn)
	    {
	      perspectiveAngle += PERSPECTIVE_INC;
	      if (perspectiveAngle > PERSPECTIVE_MAX)
	        perspectiveAngle = PERSPECTIVE_MAX;
	    }
	    projectionChanged = true; // � traiter au prochain affichage
	  }

	  /** Controle la position de l'observeur et son orientation.
	    * @param control motion type :
	    *	- TURN_RIGHT, TURN_LEFT, NO_RIGHT_LEFT_TURN,
	    *	- TURN_UP, TURN_DOWN, NO_UP_DOWN_TURN,
	    *	- GO_FRONTWARDS, GO_BACKWARDS, NO_FRONT_BACK_GO,
	    *	- GO_RIGHTWARDS, GO_LEFTWARDS, NO_RIGHT_LEFT_GO,
	    *	- GO_UPWARDS, GO_DOWNWARDS, NO_UP_DOWN_GO
	    */	
	  public void move (int control)
	  {
	    switch (control)
	    {
	      case TURN_RIGHT : azimuthMobility = -1; break;
	      case TURN_LEFT : azimuthMobility = 1; break;
	      case NO_RIGHT_LEFT_TURN : azimuthMobility = 0; break;
	      case TURN_UP : heightMobility = 1; break;
	      case TURN_DOWN : heightMobility = -1; break;
	      case NO_UP_DOWN_TURN : heightMobility = 0; break;
	      case GO_FRONTWARDS : frontMobility = 1; break;
	      case GO_BACKWARDS : frontMobility = -1; break;
	      case NO_FRONT_BACK_GO : frontMobility = 0; break;
	      case GO_RIGHTWARDS : rightMobility = 1; break;
	      case GO_LEFTWARDS : rightMobility = -1; break;
	      case NO_RIGHT_LEFT_GO : rightMobility = 0; break;
	      case GO_UPWARDS : upMobility = 1; break;
	      case GO_DOWNWARDS : upMobility = -1; break;
	      case NO_UP_DOWN_GO : upMobility = 0; break;
	    }
	  }

	  /** Reinitialise les parametres de proection actuels par defaut
	    */
	  public void resetProjection ()
	  {
	    if (perspectiveOn)
	    {
	      perspectiveAngle = DEFAULT_PERSPECTIVE_ANGLE;
	      azimuthAngle = DEFAULT_AZIMUTH_ANGLE;
	      heightAngle = DEFAULT_HEIGHT_ANGLE;
	      center[0] = defaultCenter[0];
	      center[1] = defaultCenter[1];
	      center[2] = defaultCenter[2];
	    }
	    else // orthographic
	    {
	      sceneSize = defaultSceneSize;
	      focus[0] = defaultFocus[0];
	      focus[1] = defaultFocus[1];
	    }
	    projectionChanged = true; // � traiter au prochain affichage
	  }

	  /** Definit les parametres de projection orthographique(par d�faut et les limites)
	    * @param volume defaut volume a afficher
	    *   (float array contient xmin, xmax, ymin, ymax, zmin the zmax)
	    */ 
	  public void setViewingVolume (float[] volume)
	  {
	    //La taille par defaut affiche le volume donne
	    float xSize = volume[1] - volume[0];
	    float ySize = volume[3] - volume[2];
	    if (xSize < 0.0f) xSize = - xSize;
	    if (ySize < 0.0f) ySize = - ySize;
	    defaultSceneSize = (xSize > ySize ? (double) xSize : (double) ySize);
	    sceneSize = defaultSceneSize;
	    zMin = - (double) volume[5];
	    zMax = - (double) volume[4];

	    //Le point par d�faut est le centre du volume
	    defaultFocus[0] = (volume[1] + volume[0]) / 2;
	    defaultFocus[1] = (volume[3] + volume[2]) / 2;
	    focus[0] = defaultFocus[0];
	    focus[1] = defaultFocus[1];
	  }

	  /** Renvoie les limites de l'observeur.
	    * @param volume Accessible volume limite
	    *   (float array contient xmin, xmax, ymin, ymax, zmin the zmax)
	    */ 
	  public void setAccessibleVolume (float[] volume)
	  {
	    centerXmin = volume[0];
	    centerXmax = volume[1];
	    centerYmin = volume[2];
	    centerYmax = volume[3];
	    centerZmin = volume[4];
	    centerZmax = volume[5];
	  }

	  /** Definit le centre de la perspective par defaut.
	    * @param pos Position par defaut de l'observateur.
	    */ 
	  public void setDefaultPosition (float[] pos)
	  {
	    defaultCenter[0] = pos[0];
	    defaultCenter[1] = pos[1];
	    defaultCenter[2] = pos[2];
	    center[0] = pos[0];
	    center[1] = pos[1];
	    center[2] = pos[2];
	  }
}
