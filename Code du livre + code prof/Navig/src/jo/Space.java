// Test de navigation avec butees
// Prepare par Ph. Even
package jo;
import com.jogamp.opengl.GL2;


/** Space model */
public class Space
{
  /** Hall dimensions : half the depth */
  private final float depth_2 = 4.0f;
  /** Hall dimensions : half the width */
  private final float width_2 = 3.0f;
  /** Hall dimensions : the height */
  private final float height = 4.0f;
  /** Corridor half width */
  private final float corr_2 = 1.0f;
  /** Collision distance */
  private final static float COLL = 0.1f;

  /** First light position */
  private float light1Pos[] = {0.0f, 0.0f, 2.0f, 1.0f};
  /** Second light position */
  private float light2Pos[] = {3 * depth_2, 0.0f, 2.0f, 1.0f};
  /** Light diffuse component */
  private float[] lightMat = {0.8f, 0.8f, 0.8f, 1.0f};
  /** Light specular component */
  private float[] lightSpec = {0.0f, 0.0f, 0.0f, 1.0f};
  /** Light shininess component */
  private float[] lightShininess = {1.0f};

  /** Ceiling diffuse material */
  private float[] ceilMat = {0.0f, 0.0f, 0.0f, 1.0f};
  /** Ceiling specular material */
  private float[] ceilSpec = {0.0f, 0.0f, 0.0f, 1.0f};
  /** Ceiling shininess */
  private float[] ceilShininess = {1.0f};
  /** Floor diffuse material */
  private float[] floorMat = {0.4f, 0.4f, 0.3f, 1.0f};
  /** Floor specular material */
  private float[] floorSpec = {0.5f, 0.3f, 0.1f, 1.0f};
  /** Floor shininess */
  private float[] floorShininess = {2.0f};
  /** North wall diffuse material */
  private float[] northMat = {0.2f, 0.3f, 0.1f, 1.0f};
  /** North wall specular material */
  private float[] northSpec = {0.3f, 0.4f, 0.1f, 1.0f};
  /** North wall shininess */
  private float[] northShininess = {2.0f};
  /** South wall diffuse material */
  private float[] southMat = {0.2f, 0.2f, 0.1f, 1.0f};
  /** South wall specular material */
  private float[] southSpec = {0.5f, 0.3f, 0.1f, 1.0f};
  /** South wall shininess */
  private float[] southShininess = {2.0f};
  /** East wall diffuse material */
  private float[] eastMat = {0.1f, 0.2f, 0.4f, 1.0f};
  /** East wall specular material */
  private float[] eastSpec = {0.2f, 0.3f, 0.2f, 1.0f};
  /** East wall shininess */
  private float[] eastShininess = {2.0f};
  /** West wall diffuse material */
  private float[] westMat = {0.3f, 0.1f, 0.1f, 1.0f};
  /** West wall specular material */
  private float[] westSpec = {0.5f, 0.3f, 0.1f, 1.0f};
  /** West wall shininess */
  private float[] westShininess = {2.0f};


  /** Initializes the hall.
    * @param gl GL2 context. 
    */ 
  public void init (GL2 gl)
  {
    // Lights positionning
    gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_POSITION, light1Pos, 0);
    gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_POSITION, light2Pos, 0);
  }


  /** Renders the hall.
    * @param gl GL2 context. 
    */ 
  public void draw (GL2 gl)
  {
    // Floor
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, floorMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, floorSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, floorShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, 0.0f, 1.0f);
      gl.glVertex3f (-depth_2, -width_2, 0.0f); /** permet de d�signer un sommet � utiliser dans la description d'un ojet g�om�trique.
*/    gl.glVertex3f (4 * depth_2, -width_2, 0.0f);
      gl.glVertex3f (-depth_2, width_2, 0.0f);
      gl.glVertex3f (4 * depth_2, width_2, 0.0f);
    gl.glEnd ();

    // Ceiling
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ceilMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, ceilSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, ceilShininess, 0);
    gl.glNormal3f (0.0f, 0.0f, -1.0f);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2, -width_2, height);
      gl.glVertex3f (2 * depth_2, -width_2, height);
      gl.glVertex3f (depth_2, -corr_2, height);
      gl.glVertex3f (2 * depth_2, -corr_2, height);
    gl.glEnd ();
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2, corr_2, height);
      gl.glVertex3f (2 * depth_2, corr_2, height);
      gl.glVertex3f (depth_2, width_2, height);
      gl.glVertex3f (2 * depth_2, width_2, height);
    gl.glEnd ();

    // South room
    //
    // North wall (-X : initially frontwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, northMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, northSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, northShininess, 0);
    gl.glNormal3f (1.0f, 0.0f, 0.0f);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2 * 2, -width_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, -corr_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, -width_2, height);
      gl.glVertex3f (depth_2 * 2, -corr_2, height);
    gl.glEnd ();
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2 * 2, corr_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, width_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, corr_2, height);
      gl.glVertex3f (depth_2 * 2, width_2, height);
    gl.glEnd ();

    // West wall (-Y : initially leftwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, westMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, westSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, westShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, 1.0f, 0.0f);
      gl.glVertex3f (depth_2 * 4, -width_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, -width_2, 0.0f);
      gl.glVertex3f (depth_2 * 4, -width_2, height);
      gl.glVertex3f (depth_2 * 2, -width_2, height);
    gl.glEnd ();

    // South wall (+X : initially backwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, southMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, southSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, southShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (-1.0f, 0.0f, 0.0f);
      gl.glVertex3f (depth_2 * 4, width_2, height);
      gl.glVertex3f (depth_2 * 4, width_2, 0.0f);
      gl.glVertex3f (depth_2 * 4, -width_2, height);
      gl.glVertex3f (depth_2 * 4, -width_2, 0.0f);
    gl.glEnd ();

    // East wall (+Y : initially rightwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, eastMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, eastSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, eastShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, -1.0f, 0.0f);
      gl.glVertex3f (depth_2 * 4, width_2, height);
      gl.glVertex3f (depth_2 * 2, width_2, height);
      gl.glVertex3f (depth_2 * 4, width_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, width_2, 0.0f);
    gl.glEnd ();

    // Corridor
    //
    // West wall (-Y : initially leftwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, westMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, westSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, westShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, 1.0f, 0.0f);
      gl.glVertex3f (depth_2 * 2, -corr_2, 0.0f);
      gl.glVertex3f (depth_2, -corr_2, 0.0f);
      gl.glVertex3f (depth_2 * 2, -corr_2, height);
      gl.glVertex3f (depth_2, -corr_2, height);
    gl.glEnd ();

    // East wall (+Y : initially rightwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, eastMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, eastSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, eastShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, -1.0f, 0.0f);
      gl.glVertex3f (depth_2 * 2, corr_2, height);
      gl.glVertex3f (depth_2, corr_2, height);
      gl.glVertex3f (depth_2 * 2, corr_2, 0.0f);
      gl.glVertex3f (depth_2, corr_2, 0.0f);
    gl.glEnd ();

    // North room
    //
    // North wall (-X : initially frontwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, northMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, northSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, northShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (1.0f, 0.0f, 0.0f);
      gl.glVertex3f (-depth_2, -width_2, 0.0f);
      gl.glVertex3f (-depth_2, width_2, 0.0f);
      gl.glVertex3f (-depth_2, -width_2, height);
      gl.glVertex3f (-depth_2, width_2, height);
    gl.glEnd ();

    // West wall (-Y : initially leftwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, westMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, westSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, westShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, 1.0f, 0.0f);
      gl.glVertex3f (depth_2, -width_2, 0.0f);
      gl.glVertex3f (-depth_2, -width_2, 0.0f);
      gl.glVertex3f (depth_2, -width_2, height);
      gl.glVertex3f (-depth_2, -width_2, height);
    gl.glEnd ();

    // South wall (+X : initially backwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, southMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, southSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, southShininess, 0);
    gl.glNormal3f (-1.0f, 0.0f, 0.0f);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2, -corr_2, height);
      gl.glVertex3f (depth_2, -corr_2, 0.0f);
      gl.glVertex3f (depth_2, -width_2, height);
      gl.glVertex3f (depth_2, -width_2, 0.0f);
    gl.glEnd ();
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glVertex3f (depth_2, width_2, height);
      gl.glVertex3f (depth_2, width_2, 0.0f);
      gl.glVertex3f (depth_2, corr_2, height);
      gl.glVertex3f (depth_2, corr_2, 0.0f);
    gl.glEnd ();

    // East wall (+Y : initially rightwards)
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, eastMat, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, eastSpec, 0);
    gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, eastShininess, 0);
    gl.glBegin (GL2.GL_TRIANGLE_STRIP);
      gl.glNormal3f (0.0f, -1.0f, 0.0f);
      gl.glVertex3f (depth_2, width_2, height);
      gl.glVertex3f (-depth_2, width_2, height);
      gl.glVertex3f (depth_2, width_2, 0.0f);
      gl.glVertex3f (-depth_2, width_2, 0.0f);
    gl.glEnd ();
  }

  /** Returns the visible volume.
    */
  public float[] defaultVisibleVolume ()
  {
    float[] volume = {- depth_2 * 1.5f, depth_2 * 4.5f,
                      - width_2 * 1.5f, width_2 * 4.5f,
                      - 0.1f, height * 1.5f};
    return (volume);
  }

  /** Returns the volume the observer is allowed to walk through
    */
  public float[] accessibleVolume ()
  {
    float[] volume = {- depth_2 * 0.9f, depth_2 * 3.9f,
                      - width_2 * 0.9f, width_2 * 0.9f,
                      0.1f, height - 0.1f,
                      depth_2 * 0.9f, depth_2 * 2.1f,
                      - corr_2 * 0.9f, corr_2 * 0.9f};
    return (volume);
  }

  /** Returns a relevant position of the observer in the hall.
    */
  public float[] observerPosition ()
  {
    float[] pos = {2.0f, 0.0f, 2.0f};
    return (pos);
  }
}
