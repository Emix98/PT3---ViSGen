package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import fr.visgen.model.Loadable;

/** Repr�sente une ouverture dans un mur (ex: porte, fen�tre) */
public abstract class Opening extends Selectable implements Loadable {
	/** Identifiant unique de l'ouverture */
	protected int id;
	/** Dimensions de l'ouverture */
	protected double height, width;
	/** Coordon�es de l'ouverture */
	protected double x, y;
	/** Pi�ce reli�e � l'ouverture */
	protected int linkedRoom;

	/**
	 * Construit une ouverture
	 * @param id Identifiant unique
	 * @param x Coordon�e x
	 * @param y Coordon�e y
	 * @param linkedRoom Pi�ce vers laquelle m�ne l'ouverture
	 */
	public Opening(int id, double x, double y, int linkedRoom) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.linkedRoom = linkedRoom;
	}

	/**
	 * Retourne l'identifiant unique de l'ouverture
	 * @return Identifiant
	 */
	public int getId() {
		return id;
	}
	
	
	
	// SAUVEGARDE ET CHARGEMENT
	
		@Override
		public void save(DataOutputStream data) throws IOException {
			// Ecrit l'ID
			data.writeInt(id);

			// Ecrit les coordon�es
			data.writeDouble(x);
			data.writeDouble(y);
			
			// Ecrit l'ID de la pi�ce attach�e � l'ouverture
			data.writeInt(linkedRoom);
		}

		@Override
		public void load(DataInputStream data) throws IOException {
			// Lit l'ID
			id = data.readInt();

			// Lit les coordon�es
			x = data.readDouble();
			y = data.readDouble();
			
			// Lit l'ID de la pi�ce attach�e � l'ouverture
			linkedRoom = data.readInt();
		}
}
