package fr.visgen.view.modeller;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Opening;
import fr.visgen.model.elements.Point;

public class ModellerRoomView extends JPanel implements Observer {

	private static final long serialVersionUID = -3300263945233505237L;

	/** Mod�le, contenant entre autres la pi�ce actuellement s�lectionn�e */
	private ModellerModel modellerModel;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(Color.BLACK);

		drawWalls(g);
	}

	/**
	 * Affiche les murs de la pi�ce
	 * @param g Contexte graphique de la vue
	 */
	private void drawWalls(Graphics g) {
		List<Point> summits = modellerModel.getCurrentArea().getSummits();

		for(int i = 0; i < summits.size() - 1; i++) {
			// Point 1
			int x1 = (int) summits.get(i).getX() * ModellerModel.getScale() + ModellerModel.getMargin();
			int y1 = (int) summits.get(i).getY() * ModellerModel.getScale() + ModellerModel.getMargin();

			// Point 2
			int x2 = (int) summits.get(i + 1).getX() * ModellerModel.getScale() + ModellerModel.getMargin();
			int y2 = (int) summits.get(i + 1).getY() * ModellerModel.getScale() + ModellerModel.getMargin();

			// On dessine le mur
			g.drawLine(x1, y1, x2, y2);
		}

		// On affiche le dernier mur
		// Point 1
		int x1 = (int) summits.get(0).getX() * ModellerModel.getScale() + ModellerModel.getMargin();
		int y1 = (int) summits.get(0).getY() * ModellerModel.getScale() + ModellerModel.getMargin();

		// Point 2
		int x2 = (int) summits.get(summits.size() - 1).getX() * ModellerModel.getScale() + ModellerModel.getMargin();
		int y2 = (int) summits.get(summits.size() - 1).getY() * ModellerModel.getScale() + ModellerModel.getMargin();


		g.drawLine(x1, y1, x2, y2);
	}

	private void drawOpenings(Graphics g) {
		List<Point> summits = modellerModel.getCurrentArea().getSummits();
		List<Opening> openings = modellerModel.getOpeningList();

		// TODO Dessiner les ouvertures en fonction du mur auquel elles appartiennent
	}

	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

	public ModellerRoomView(ModellerModel modellerModel) {
		super();
		this.modellerModel = modellerModel;
	}

}
