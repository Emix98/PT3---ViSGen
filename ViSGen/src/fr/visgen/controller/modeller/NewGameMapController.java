package fr.visgen.controller.modeller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.visgen.model.GameMap;
import fr.visgen.model.ModellerModel;

public class NewGameMapController implements ActionListener  {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;

	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application
	 * @param model Mod�le de l'application
	 */
	public NewGameMapController(ModellerModel model) 
	{
		modellerModel = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		modellerModel.setMap(new GameMap());
		modellerModel.addRoom();
	}
}
