package fr.visgen.controller.modeller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.MapArea;
import fr.visgen.view.modeller.ModellerTreeView;

public class TreeViewController implements MouseListener {
	
	/** Mod�le de l'application */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerTreeView frame;
	/** Arborescence de l'application */
	private JTree tree;
	
	// CONSTRUCTEURS
	public TreeViewController(ModellerModel modellerModel, ModellerTreeView frame) {
		this.modellerModel = modellerModel;
		this.frame = frame;
		
		tree = frame.getMapTree();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO r�gler la lev�e d'exception
		// TODO Finir cet algorithme
		if(e.getSource() instanceof JTree) {
			TreePath path = tree.getPathForLocation(e.getX(), e.getY());
			System.out.println(path);
			String name = (String) path.getPathComponent(1);
			
			for(MapArea area : modellerModel.getRoomList()) {
				if(area.getName().equals(name)) {
					modellerModel.setCurrentArea(area);
				}
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) { }

	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }

	@Override
	public void mouseReleased(MouseEvent e) { }

}
