package fr.visgen.model.elements;

import java.util.ArrayList;
import java.util.List;

/** Repr�sente une salle du labyrinthe */
public class Room extends MapArea {
	
	/** Murs de la pi�ce */
	private List<Wall> walls;
	
	/** 
	 * Construit une pi�ce en donnant une forme et une liste des ouvertures
	 * @param summits Sommets du polygone repr�sentant la forme de la pi�ce
	 * @param openings Liste d'ouvertures de la pi�ce
	 */
	public Room(int id, List<Point> summits, List<Opening> openings) {
		super(id, summits, openings);
		// Nom par d�faut
		name = "Pi�ce sans nom ";
	}

	/** 
	 * Construit une nouvelle pi�ce en fournissant son id 
	 * @param id Identifiant de la pi�ce
	 */
	public Room(int id) {
		super(id);

		// Nom par d�faut
		name = "Pi�ce sans nom "+id;

		// G�n�re la salle par d�faut
		// Cr�e les points
		Point[] pointList = new Point[] {
				new Point(0, 7),
				new Point(16, 7),
				new Point(16, -10),
				new Point(0, -10)
		};

		// Cr�e la liste des sommets de la pi�ce
		summits = new ArrayList<Point>();

		// Ajoute les points � la liste
		for(Point p : pointList) {
			summits.add(p);
		}
		
		// Cr�e les murs de la pi�ce
		walls = new ArrayList<Wall> ();
		generateWalls();

		// Cr�e 2 portes et les ajoute
		Opening[] openings = new Opening[] {
				new Door(0, 0, 0, DEAD_END),
				new Door(1, 16, 3, DEAD_END),
				new Window(0, 8, 7, DEAD_END)
		};

		// Ajoute les portes et la fen�tre � la salle par d�faut
		for(Opening o : openings) {
			this.openings.add(o);
		}
	}
	
	/**
	 * Ajoute un sommet � la pi�ce
	 * @param summit Sommet � rajouter
	 */
	public void addSummit(Point summit) {
		summits.add(summit);
		generateWalls();
	}
	
	/**
	 * G�n�re les murs de la pi�ce
	 */
	public void generateWalls() {
		
		// On recr�e la liste des murs (on repart de z�ro)
		walls = new ArrayList<Wall>();
		
		for(Point p : summits) {
			
			if ( summits.indexOf(p)+1 < summits.size() ) {
				walls.add( new Wall( p, summits.get( summits.indexOf(p)+1 ) ) );
			} else {
				//Ajout du dernier mur, reliant le dernier sommet de la pi�ce au premier
				walls.add( new Wall( p, summits.get(0) ) );
			}
			
		}
		
	}

	/**
	 * Permet de d�finir le nom de la salle
	 * @param name Nom de la salle
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * R�cup�re les murs constituant la salle
	 * @return walls Liste des murs de la pi�ce
	 */
	public List<Wall> getWalls() {
		return walls;
	}
}