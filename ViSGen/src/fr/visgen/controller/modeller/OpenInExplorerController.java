package fr.visgen.controller.modeller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.visgen.model.ModellerModel;
import fr.visgen.view.explorer.ExplorerFrame;
import fr.visgen.view.modeller.ModellerFrame;


public class OpenInExplorerController implements ActionListener  {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerFrame frame;
	

	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application
	 * @param model Mod�le de l'application
	 */
	public OpenInExplorerController(ModellerModel model, ModellerFrame frame) 
	{
		modellerModel = model;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		ExplorerFrame e = new ExplorerFrame("ViSGen Explorer", modellerModel.getMap());
		frame.dispose();
	}
}
