package fr.visgen.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Observable;

import fr.visgen.model.elements.Corridor;
import fr.visgen.model.elements.MapArea;
import fr.visgen.model.elements.Opening;
import fr.visgen.model.elements.Point;
import fr.visgen.model.elements.Room;
import fr.visgen.model.elements.Selectable;

/** Mod�le concret du modeleur */
public class ModellerModel extends Observable {
	/** Version du logiciel */
	private static int version = 1;

	/** Contient les �l�ments de la carte actuelle */
	private GameMap map;

	/** Pi�ce actuellement en cours d'�dition */
	private MapArea currentArea;

	/** �l�ment s�lectionn� par l'utilisateur */
	private Selectable selectedElement;
	
	/** Permet de v�rifier si un �l�ment est en placement */
	private boolean inPlacement;

	/** Marge de l'affichage */
	private final static int MARGIN = 200;
	/** Echelle de l'affichage */
	private final static int SCALE = 10;



	// CONSTRUCTEURS
	/** Construit un mod�le avec une carte vide par d�faut */
	public ModellerModel() {
		this(new GameMap());
	}

	/**
	 * Construit un mod�le contenant une carte pr�-initialis�e 
	 * @param map Carte pr�-initialis�e 
	 */
	public ModellerModel(GameMap map) {
		this.map = map;
		
		inPlacement = false;

		// Cr�e une pi�ce par d�faut
		addRoom();
	}



	// METHODES
	/** Force les vues � se rafra�chir */
	public void refresh() {
		// On notifie les observateurs du changement
		setChanged();
		notifyObservers();
	}


	// GETTERS / SETTERS

	/**
	 * Retourne la carte (GameMap) actuelle
	 * @return Carte actuelle
	 */
	public GameMap getMap() {
		return map;
	}

	/**
	 * D�finit la carte actuelle
	 * @param map Carte � modifier
	 */
	public void setMap(GameMap map) {
		this.map = map;

		// On notifie les observateurs du changement
		refresh();
	}

	/** Retourne la zone actuellement ouverte 
	 * @return Zone actuellement ouverte
	 */
	public MapArea getCurrentArea() {
		return currentArea;
	}

	/** D�finit la zone actuellement ouverte
	 * @param currentArea Salle � d�finir comme carte active
	 */
	public void setCurrentArea(MapArea currentArea) {
		this.currentArea = currentArea;

		// On notifie les observateurs du changement
		refresh();
	}

	/** Retourne la version du logiciel 
	 * @return Version du logiciel
	 */
	public static int getVersion() {
		return version;
	}

	/**
	 * Retourne l'�l�ment s�lectionn� par l'utilisateur
	 * @return Selectable �l�ment s�lectionn� par l'utilisateur
	 */
	public Selectable getSelectedElement() {
		return selectedElement;
	}

	/**
	 * D�finit l'�l�ment s�lectionn� par l'utilisateur
	 * @param selectedElement �l�ment s�lectionn�
	 */
	public void setSelectedElement(Selectable selectedElement) {
		this.selectedElement = selectedElement;

		// On notifie les observateurs du changement
		refresh();
	}

	/** Retourne la liste des pi�ces de la carte 
	 * @return Liste des pi�ces
	 * */
	public List<MapArea> getRoomList() {
		return map.getRoomList();
	}
	
	/**
	 * Retourne le bool�en permettant de v�rifier si un �l�ment est en placement
	 * @return inPlacement Bool�en permettant de v�rifier si un �l�ment est en placement
	 */
	public boolean isInPlacement() {
		return inPlacement;
	}

	/**
	 * Permet de d�finir si un �l�ment est en placement par l'utilisateur
	 * @param inPlacement Bool�en permettant de d�finir si un �l�ment est en placement par l'utilisateur
	 */
	public void setInPlacement(boolean inPlacement) {
		this.inPlacement = inPlacement;
	}

	/** 
	 * Ajoute une pi�ce � la carte
	 * @param r Pi�ce � ajouter � la carte 
	 */
	public void addMapArea(MapArea r) {
		map.getRoomList().add(r);
		currentArea = r;

		// On notifie les observateurs du changement
		refresh();
	}

	/** Ajoute une nouvelle salle � la carte en lui attribuant automatiquement un ID */
	public void addRoom() {
		addMapArea(new Room(map.getRoomList().size()));
	}

	/** Ajoute un couloir � la carte en lui attribuant automatiquement un ID */
	public void addCorridor() {
		addMapArea(new Corridor(map.getRoomList().size()));
	}

	/** 
	 * Ajoute une ouverture (porte ou fen�tre) � la salle
	 * courante.
	 * @param opening Ouverture � ajouter 
	 */
	public void addOpening(Opening opening) {
		currentArea.getOpeningList().add((Opening) opening);
		
		// On notifie les observateurs du changement
		refresh();
	}

	/**
	 * Ajoute un sommet � la pi�ce ou au couloir actuel
	 * @param summit Sommet � ajouter
	 */
	public void addSummit(Point summit) {
		System.out.println(currentArea.getSummits());
		currentArea.addSummit(summit);
		System.out.println(currentArea.getSummits());
		// On notifie les observateurs du changement
		refresh();
	}

	/**
	 * Retourne la marge de l'affichage
	 * @return Marge de l'affichage
	 */
	public static int getMargin() {
		return MARGIN;
	}

	/**
	 * Retourne l'�chelle de l'affichage
	 * @return Echelle de l'affichage
	 */
	public static int getScale() {
		return SCALE;
	}

	/**
	 * Retourne la liste des ouvertures � la salle courante.
	 * @return 
	 * @return Liste des ouvertures.
	 */
	public List<Opening> getOpeningList() {
		return currentArea.getOpeningList();
	}



	// SAUVEGARDE / CHARGEMENT
	public void save(File file) {
		try {
			FileOutputStream fos = new FileOutputStream(file);
			DataOutputStream outStream = new DataOutputStream(fos);
			map.save(file, outStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void load(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			DataInputStream inStream = new DataInputStream(fis);
			map.load(file, inStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// On notifie les observateurs du changement
		refresh();
	}
}
