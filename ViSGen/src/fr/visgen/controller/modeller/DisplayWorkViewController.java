package fr.visgen.controller.modeller;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.visgen.view.modeller.ModellerFrame;

public class DisplayWorkViewController implements ActionListener  {
	/** Fen�tre de l'application */
	private ModellerFrame frame;

	/**
	 * Construit le contr�leur � l'aide de la fen�tre de l'application
	 * @param frame Fen�tre de l'application
	 */
	public DisplayWorkViewController(ModellerFrame frame) 
	{
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// On retire l'ancienne vue
		frame.remove(frame.getCurrentView());
		
		// On bascule la vue
		if (frame.getCurrentView() != frame.getRoomView()) frame.setCurrentView(frame.getRoomView());
		else frame.setCurrentView(frame.getGraphView());
		
		// On ajoute la nouvelle vue
		frame.add(frame.getCurrentView(), BorderLayout.CENTER);

		// On rafra�chit l'affichage
		frame.revalidate();
		frame.repaint();
	}
}
