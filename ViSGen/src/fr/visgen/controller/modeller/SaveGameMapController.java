package fr.visgen.controller.modeller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.visgen.model.ModellerModel;
import fr.visgen.view.modeller.ModellerFrame;

public class SaveGameMapController implements ActionListener {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerFrame frame;
	/** Fen�tre permettant de choisir l'emplacement du fichier � sauvegarder */
	private JFileChooser fileChooser;

	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application
	 * @param model Mod�le de l'application
	 * @param frame Fen�tre de l'application
	 */
	public SaveGameMapController(ModellerModel model, ModellerFrame frame) 
	{
		modellerModel = model;
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (fileChooser == null) fileChooser = new JFileChooser(new File(System.getProperty("user.dir")));
		FileNameExtensionFilter filter = new FileNameExtensionFilter ("Projets ViSGen (.vsg)", "vsg");
		fileChooser.setFileFilter(filter);
		int returnVal = fileChooser.showSaveDialog(frame.getParent());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File savedFile = fileChooser.getSelectedFile();
			if(!savedFile.getPath().contains(".vsg")) savedFile = new File(savedFile.getPath() + ".vsg");
			modellerModel.save(savedFile);
		}
	}

}
