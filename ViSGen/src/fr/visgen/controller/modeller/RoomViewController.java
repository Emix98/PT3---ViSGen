package fr.visgen.controller.modeller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Point;
import fr.visgen.model.elements.Room;
import fr.visgen.model.elements.Wall;
import fr.visgen.view.modeller.ModellerRoomView;

public class RoomViewController implements MouseListener {
	/** Mod�le de l'application */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerRoomView frame;

	/** Coordon�e X du clic */
	private int x;
	/** Coordon�e Y du clic */
	private int y;
	/** Coordonn�e X d'origine du clic. Sert � calculer les vecteurs de d�placement pour les murs */
	private int initX;
	/** Coordonn�e Y d'origine du clic. Sert � calculer les vecteurs de d�placement pour les murs */
	private int initY;
	/** Indique si l'utilisateur d�place un point */
	private boolean accroc = false;

	// CONSTRUCTEURS
	public RoomViewController(ModellerModel modellerModel, ModellerRoomView frame) {
		this.modellerModel = modellerModel;
		this.frame = frame;
	}

	// EVENEMENTS
	public void mouseClicked(MouseEvent event) { }

	@Override
	public void mouseEntered(MouseEvent event) { }

	@Override
	public void mouseExited(MouseEvent event) { }

	@Override
	public void mousePressed(MouseEvent event) {
		x = event.getX();
		initX = (x - ModellerModel.getMargin()) / ModellerModel.getScale();
		y = event.getY();
		initY = (y - ModellerModel.getMargin()) / ModellerModel.getScale();

		selectItem( (x - ModellerModel.getMargin()) / ModellerModel.getScale(), (y - ModellerModel.getMargin()) / ModellerModel.getScale() );
		System.out.println(modellerModel.getSelectedElement());
	}

	@Override
	public void mouseReleased(MouseEvent event) { 
		x = event.getX();
		y = event.getY();
		
		if (event.getButton() == event.BUTTON3 && modellerModel.getSelectedElement() != null && modellerModel.getCurrentArea().getSummits().size() > 3) {
			List<Point> currentSummits = modellerModel.getCurrentArea().getSummits();
			currentSummits.remove(currentSummits.indexOf(modellerModel.getSelectedElement()));
			//On reg�n�re les murs de la pi�ce
			if (modellerModel.getCurrentArea() instanceof Room) {
				Room room = (Room) modellerModel.getSelectedElement();
				room.generateWalls();
			}
		}
		else if (accroc && modellerModel.getSelectedElement() != null) moveItem((x - ModellerModel.getMargin()) / ModellerModel.getScale(), (y - ModellerModel.getMargin()) / ModellerModel.getScale());
	    accroc = false;
	    frame.repaint ();
	}
	
	/** D�tecte si un objet (mur, point, porte...) a �t� s�lectionn� */
	public boolean selectItem(int x, int y) {
		
		//On teste tout d'abord si un point a �t� s�lectionn� par l'utilisateur
		List<Point> summits = modellerModel.getCurrentArea().getSummits();
		for (Point p : summits) {
			
			if ( x >= p.getX()-2 && x < p.getX()+2 && y >= p.getY()-2 && y < p.getY()+2 ) {
				modellerModel.setSelectedElement(p);
				accroc = true;
				return true;
			}
			
		}
		
		//Si aucun point n'a �t� s�lectionn�, on teste si l'utilisateur a s�lectionn� un mur
		List<Wall> walls = modellerModel.getCurrentArea().getWalls();
		double pos;
		double[] relPos;
		
		for (Wall w : walls) {
			
			pos = w.getEquation()[0]*x + w.getEquation()[1]*y + w.getEquation()[2]; //Plus la valeur obtenue est proche de 0, plus on est proche de la droite
			relPos = w.getRelPos();
			
			/*&& x > (relPos[0] >=0 ? w.getP1().getX() : w.getP2().getX()) && x < (relPos[0] >=0 ? w.getP2().getX() : w.getP1().getX()) &&
			  y > (relPos[1] >=0 ? w.getP1().getY() : w.getP2().getY()) && y < (relPos[1] >=0 ? w.getP2().getY() : w.getP1().getY())*/
			
			if ( pos >= -2 && pos <= 2 ) {
				modellerModel.setSelectedElement(w);
				accroc = true;
				return true;
			}
			
		}
		
		accroc = false;
		modellerModel.setSelectedElement(null);
		return false;
		
	}
	
	/** D�place un objet */
	public void moveItem(int x, int y) {
		if (modellerModel.getSelectedElement() instanceof Point) {
			Point p = (Point) modellerModel.getSelectedElement();
			p.setX(x);
			p.setY(y);
		}
		if (modellerModel.getSelectedElement() instanceof Wall) {
			Wall w = (Wall) modellerModel.getSelectedElement();
			//Calcul du d�placement du mur
			int dx = x - initX;
			int dy = y - initY;
			//Application du d�placement aux points du mur
			w.getP1().setX(w.getP1().getX()+dx);
			w.getP1().setY(w.getP1().getY()+dy);
			w.getP2().setX(w.getP2().getX()+dx);
			w.getP2().setY(w.getP2().getY()+dy);
			//Reg�n�ration des murs
			Room r = (Room) modellerModel.getCurrentArea();
			r.generateWalls();
		}
	}
}
