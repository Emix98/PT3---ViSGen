package fr.visgen.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.visgen.model.elements.Corridor;
import fr.visgen.model.elements.MapArea;
import fr.visgen.model.elements.Room;

/** Repr�sente la carte du jeu (ensemble des salles et des �l�ments) */
public class GameMap {
	/** Nom de la GameMap */
	private String name;
	/** Contient toutes les salles, la clef correspond � l'identifiant de la salle */
	private List<MapArea> roomList;

	/**
	 * Construit une GameMap par d�faut
	 */
	public GameMap() {
		this("Carte sans nom");
	}

	public GameMap(String name) {
		this.name = name;
		roomList = new ArrayList<MapArea>();
	}


	// GETTERS ET SETTERS

	/** Retourne la liste des pi�ces de la carte 
	 * @return roomList Liste des pi�ces
	 */
	public List<MapArea> getRoomList() {
		return roomList;
	}

	/**
	 * Retourne le nom de la carte
	 * @return Nom de la carte
	 */
	public String getName() {
		return name;
	}

	/**
	 * Permet de d�finir le nom de la carte
	 * @param name Nouveau nom de la carte
	 */
	public void setName(String name) {
		this.name = name;
	}


	// METHODES DE SAUVEGARDE ET DE CHARGEMENT
	public void save(File file, DataOutputStream data) {

		try {
			// D�but de l'enregistrement
			// Enregistre la version du modeleur dans le fichier
			data.writeInt(ModellerModel.getVersion());

			// Ecrit la taille du nom (en caract�res)
			data.writeInt(name.length());
			
			// Ecrit le nom
			data.writeChars(name);
			
			// Enregistre le nombre de salles
			data.writeInt(roomList.size());

			// Sauvegarde toutes les salles
			for(int i = 0; i < roomList.size(); i++) {
				// Type: 0 = Room, 1 = Corridor
				if(roomList.get(i) instanceof Room) data.writeInt(0);
				else if(roomList.get(i) instanceof Corridor) data.writeInt(1);
				
				roomList.get(i).save(data);
			}

			data.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void load(File file, DataInputStream data) {
		try {
			// Vide la liste des salles
			roomList = new ArrayList<MapArea>();

			// D�but du chargement
			int version = data.readInt();
			
			// Charge la taille du nom
			int size = data.readInt();
			
			// Charge le nom
			for(int i = 0; i < size; i++) {
				name += data.readChar();
			}
			
			int roomNbr = data.readInt();
			
			// Charge les salles
			for(int i = 0; i < roomNbr; i++) {
				// Pi�ce 0, couloir 1
				int type = data.readInt();
				
				System.out.println("Type" + type);

				if(type == 0) roomList.add(new Room(roomList.size()));
				else roomList.add(new Corridor(roomList.size()));
				roomList.get(i).load(data);
			}

			data.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}


}
