package fr.visgen.view.explorer;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import com.jogamp.opengl.GL2;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.MapArea;
import fr.visgen.model.elements.Point;
import fr.visgen.model.elements.Wall;
import fr.visgen.view.modeller.ModellerRoomView;

public class ViewArea3DExplorer {

	/** Hall dimensions : half the depth */
	private final float depth_2 = 4.0f;
	/** Hall dimensions : half the width */
	private final float width_2 = 3.0f;
	/** Hall dimensions : the height */
	private final float height = 4.0f;
	/** Corridor half width */
	private final float corr_2 = 1.0f;
	/** Collision distance */
	private final static float COLL = 0.1f;

	/** First light position */
	private float light1Pos[] = {0.0f, 0.0f, 2.0f, 1.0f};
	/** Second light position */
	private float light2Pos[] = {0f, 17f, 2.0f, 1.0f};
	/** Third light position */
	private float light3Pos[] = {16f, 17f, 2.0f, 1.0f};
	/** Four light position */
	private float light4Pos[] = {16f, 0f, 2.0f, 1.0f};
	/** Five light position */
	private float light5Pos[] = {8.0f, 8.5f, 2.0f, 1.0f};
	/** Light diffuse component */
	private float[] lightMat = {0.8f, 0.8f, 0.8f, 1.0f};
	/** Light specular component */
	private float[] lightSpec = {0.0f, 0.0f, 0.0f, 1.0f};
	/** Light shininess component */
	private float[] lightShininess = {1.0f};

	/** Ceiling diffuse material */
	private float[] ceilMat = {0.0f, 0.0f, 0.0f, 1.0f};
	/** Ceiling specular material */
	private float[] ceilSpec = {0.0f, 0.0f, 0.0f, 1.0f};
	/** Ceiling shininess */
	private float[] ceilShininess = {1.0f};
	/** Floor diffuse material */
	private float[] floorMat = {0.4f, 0.4f, 0.3f, 1.0f};
	/** Floor specular material */
	private float[] floorSpec = {0.5f, 0.3f, 0.1f, 1.0f};
	/** Floor shininess */
	private float[] floorShininess = {2.0f};
	/** North wall diffuse material */
	private float[] northMat = {0.2f, 0.3f, 0.1f, 1.0f};
	/** North wall specular material */
	private float[] northSpec = {0.3f, 0.4f, 0.1f, 1.0f};
	/** North wall shininess */
	private float[] northShininess = {2.0f};
	/** South wall diffuse material */
	private float[] southMat = {0.2f, 0.2f, 0.1f, 1.0f};
	/** South wall specular material */
	private float[] southSpec = {0.5f, 0.3f, 0.1f, 1.0f};
	/** South wall shininess */
	private float[] southShininess = {2.0f};
	/** East wall diffuse material */
	private float[] eastMat = {0.1f, 0.2f, 0.4f, 1.0f};
	/** East wall specular material */
	private float[] eastSpec = {0.2f, 0.3f, 0.2f, 1.0f};
	/** East wall shininess */
	private float[] eastShininess = {2.0f};
	/** West wall diffuse material */
	private float[] westMat = {0.3f, 0.1f, 0.1f, 1.0f};
	/** West wall specular material */
	private float[] westSpec = {0.5f, 0.3f, 0.1f, 1.0f};
	/** West wall shininess */
	private float[] westShininess = {2.0f};

	ModellerModel modellerModel = new ModellerModel();
	/**Contient la lsite de point pour le sol**/
	List<Point> summits = modellerModel.getCurrentArea().getSummits();
	/**Contient la lsite de point pour les murs**/
	List<Wall> wall = modellerModel.getCurrentArea().getWalls();

	
	int xmin=0;
	int ymin=0;
	
	int xmax=0;
	int ymax=0;
	
	/** Initialise le hall.
	 * @param gl GL2 context. 
	 */ 
	public void init (GL2 gl)
	{
		// Positionnement des lumieres
		gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_POSITION, light1Pos, 0);
		gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_POSITION, light2Pos, 0);
		gl.glLightfv (GL2.GL_LIGHT2, GL2.GL_POSITION, light3Pos, 0);
		gl.glLightfv (GL2.GL_LIGHT3, GL2.GL_POSITION, light4Pos, 0);
		gl.glLightfv (GL2.GL_LIGHT4, GL2.GL_POSITION, light5Pos, 0);
	}

	
	
	/** @param gl GL2 context. 
	 */ 
	public void draw (GL2 gl)
	{
		// Recupere les coordonnees minimum du point
		for(Point p : summits) {
			if (p.getX()<xmin)
				xmin=(int) p.getX()*-1;
			if (p.getY()<ymin)
				ymin=(int) p.getY()*-1;
		}
		
		// Recupere les coordonnees maximum du point
		for(Point p : summits) {
			if (p.getX()>xmax)
				xmax=(int) p.getX();
			if (p.getY()>ymax)
				ymax=(int) p.getY();
		}
		
		// Sol de la piece
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, floorMat, 0);
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, floorSpec, 0);
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, floorShininess, 0);
		gl.glBegin (GL2.GL_TRIANGLE_STRIP);
		
		for (int i = 0 ; i<summits.size();i++) {
				gl.glVertex3d (summits.get(i).getX()+xmin, summits.get(i).getY()+ymin, 0.0f);
		}
		
		for (int i = 0 ; i<summits.size();i++) {
			gl.glVertex3d (summits.get(i).getX()+xmin, summits.get(i).getY()+ymin, 0.0f);
		}
		gl.glEnd();
		
		//Murs des pieces
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, westMat, 0);
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SPECULAR, westSpec, 0);
		gl.glMaterialfv (GL2.GL_FRONT, GL2.GL_SHININESS, westShininess, 0);
		gl.glBegin (GL2.GL_TRIANGLE_STRIP);
		for (int i = 0 ; i<wall.size();i++) {
			gl.glVertex3d (wall.get(i).getP1().getX()+xmin, wall.get(i).getP1().getY()+ymin, 0.0f);
			gl.glVertex3d (wall.get(i).getP1().getX()+xmin, wall.get(i).getP1().getY()+ymin, 3.0f);
			gl.glVertex3d (wall.get(i).getP2().getX()+xmin, wall.get(i).getP2().getY()+ymin, 3.0f);
			gl.glVertex3d (wall.get(i).getP2().getX()+xmin, wall.get(i).getP2().getY()+ymin, 0.0f);
			gl.glVertex3d (wall.get(i).getP2().getX()+xmin, wall.get(i).getP2().getY()+ymin, 0.0f);
			gl.glVertex3d (wall.get(i).getP2().getX()+xmin, wall.get(i).getP2().getY()+ymin, 3.0f);
			gl.glVertex3d (wall.get(i).getP1().getX()+xmin, wall.get(i).getP1().getY()+ymin, 3.0f);
			gl.glVertex3d (wall.get(i).getP1().getX()+xmin, wall.get(i).getP1().getY()+ymin, 0.0f);
		}
		
		gl.glEnd ();
	}
	/** Retourne le volume visible.
	 */

	public float[] defaultVisibleVolume ()
	{
		// Recupere les coordonnees minimum du point
		for(Point p : summits) {
			if (p.getX()<xmin)
				xmin=(int) p.getX()*-1;
			if (p.getY()<ymin)
				ymin=(int) p.getY()*-1;
		}
		
		// Recupere les coordonnees maximum du point
		for(Point p : summits) {
			if (p.getX()>xmax)
				xmax=(int) p.getX();
			if (p.getY()>ymax)
				ymax=(int) p.getY();
		}
		float[] volume = {xmin, xmax,
				ymin,ymax,
				- 0.1f, height * 1.5f};
		return (volume);
	}

	/** Retourne le volume dans lequel l'observeur peut marcher
	 */
	public float[] accessibleVolumepiece1 ()
	{
				// Recupere les coordonnees minimum du point
				for(Point p : summits) {
					if (p.getX()<xmin)
						xmin=(int) p.getX()*-1;
					if (p.getY()<ymin)
						ymin=(int) p.getY()*-1;
				}
				
				// Recupere les coordonnees maximum du point
				for(Point p : summits) {
					if (p.getX()>xmax)
						xmax=(int) p.getX();
					if (p.getY()>ymax)
						ymax=(int) p.getY();
				}
		
		float[] volume = {0.1f, xmax + xmin-0.1f,
				0.1f , ymax+ymin-0.1f,
				0.1f, height - 0.1f
				/** depth_2 * 0.9f, depth_2 * 2.1f,
	                      - corr_2 * 0.9f, corr_2 * 0.9f*/};
		return (volume);
	}

	/** Retourne l'emplacement de l'apparition de l'observeur
	 */
	public float[] observerPosition ()
	{
		float[] pos = {4.0f, 8.0f, 2.0f};
		return (pos);
	}

}
