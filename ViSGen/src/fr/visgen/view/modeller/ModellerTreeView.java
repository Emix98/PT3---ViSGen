package fr.visgen.view.modeller;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.visgen.controller.modeller.TreeViewController;
import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Door;
import fr.visgen.model.elements.MapArea;
import fr.visgen.model.elements.Opening;

public class ModellerTreeView extends JPanel implements Observer {
	
	private static final long serialVersionUID = 1601165093136704150L;
	
	/** Mod�le, contenant entre autres la GameMap actuellement �dit�e */
	private ModellerModel modellerModel;
	
	private JTree componentsTree;
	private JScrollPane treeView;

	public ModellerTreeView(ModellerModel modellerModel) {
		this.modellerModel = modellerModel;
		generateMapTree();
	}

	@Override
	public void update(Observable o, Object arg) {
		generateMapTree();
		
		revalidate();
		repaint();
	}
	
	/** G�n�re l'arbre de la carte actuelle */
	public void generateMapTree() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(modellerModel.getMap().getName());
		
		List<MapArea> curMapRoomList = modellerModel.getMap().getRoomList();
		List<Opening> curRoomOpeningsList;
		DefaultMutableTreeNode currentArea, currentOpening;
		
		for (MapArea area : curMapRoomList) {
			currentArea = new DefaultMutableTreeNode(area.getName());
			
			curRoomOpeningsList = area.getOpeningList();
			for (Opening opening : curRoomOpeningsList) {
				currentOpening = new DefaultMutableTreeNode( (opening instanceof Door ? "Porte n�" : "Fenetre n�") + opening.getId() );
				currentArea.add(currentOpening);
			}
			
			root.add(currentArea);
		}
		if(componentsTree != null) {
			remove(treeView);
		}
		componentsTree = new JTree(root);
		
		componentsTree.addMouseListener(new TreeViewController(modellerModel, this));
		
		treeView = new JScrollPane(componentsTree);
		add(treeView);
	}
	
	/** R�cup�re l'arbre de la carte actuelle */
	public JTree getMapTree() {
		return componentsTree;
	}
	
}
