// Test de navigation avec butees
// Prepare par Ph. Even
package fr.visgen.model;
import com.jogamp.opengl.GL2;


/** Box solid primitive */
public class Box extends SolidPrimitive
{
  /** Half depth (X) */
  private float depth_2 = 1.0f;
  /** Half width (Y) */
  private float width_2 = 1.0f;
  /** Half height (Z) */
  private float height_2 = 1.0f;

  public Box (float depth, float width, float height, int ref)
  {
    depth_2 = depth / 2; 
    width_2 = width / 2; 
    height_2 = height / 2; 
    setReference (ref);
  }

  public Box (float depth, float width, float height)
  {
    this (depth, width, height, REF_BASE);
  }

  /** Construit une unite Box
    */ 
  public Box ()
  {
  }

  /** Definit le systeme de reference de Box (REF_CENTER or REF_BASE)
    * @param ref Reference system position wrt the box
    */
  protected void setReference (int ref)
  {
    switch (ref)
    {
      case REF_CENTER :
        refMatrix[11] = 0.0f;
        break;
      case REF_BASE :
        refMatrix[14] = height_2;
        break;
      default :
        break;
    }
  }

@Override
public void draw(GL2 gl) {
	// TODO Auto-generated method stub
	
}
}
