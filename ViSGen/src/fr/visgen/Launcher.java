package fr.visgen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Room;
import fr.visgen.view.explorer.ExplorerFrame;
import fr.visgen.view.modeller.ModellerFrame;

/** Fen�tre du launcher */
public class Launcher extends JFrame {
	private static final long serialVersionUID = -6902547733665886976L;

	/** Classe interne contenant le logo */
	class Logo extends JPanel {
		private static final long serialVersionUID = 8911652454761220043L;
		/** Image du logo */
		private Image logo;
		
		public Logo() {
			try {
				logo = ImageIO.read(getClass().getResource("img/logo.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(logo, 0, 0, this);
		}
	}

	/** Couleur globale de la fen�tre */
	private final Color BACKGROUND_COLOR = Color.WHITE;
	
	// DIMENSIONS
	/** Taille du logo */
	private final Dimension LOGO_DIMENSION = new Dimension(360, 360);
	/** Taille de base des boutons */
	private final Dimension BUTTON_SIZE = new Dimension(400, 50);

	/** Panel principal */
	private JPanel mainPanel = new JPanel();
	/** Panel du menu */
	private JPanel menuPanel = new JPanel();
	/** Panel contenant le logo */
	private Logo logo = new Logo();

	// LABELS
	/** Label de titre */
	private JLabel menuLabel = new JLabel("Choisissez une action :", SwingConstants.CENTER);

	// BOUTONS
	/** Bouton servant � ouvrir le modeleur */
	private JButton modellerButton = new JButton("Ouvrir le modeleur");
	/** Bouton servant � ouvrir l'explorateur */
	private JButton explorerButton = new JButton("Ouvrir l'explorateur");
	/** Bouton servant � ouvrir la documentation */
	private JButton docButton = new JButton("Ouvrir la documentation");
	


	/** Par d�faut, construit une fen�tre ViSGen launcher */
	public Launcher() throws HeadlessException {
		this("ViSGen Launcher");
	}

	/** Constructeur prenant le nom de l'application en param�tre */
	public Launcher(String name) throws HeadlessException {
		super(name);
		
		// D�finition des listeners
		modellerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {

					@Override
					public void run() {
						@SuppressWarnings("unused")
						ModellerFrame m = new ModellerFrame();
						dispose();
					}
					
				});
			}
			
		});

		explorerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {

					@Override
					public void run() {
						@SuppressWarnings("unused")
						ExplorerFrame e = new ExplorerFrame("ViSGen Explorer");
						dispose();
					}
					
				});
			}
			
		});
		
		docButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(new File("PT3---ViSGen/ViSGen/doc/index.html"));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
		});

		// Taille des �l�ments
		explorerButton.setPreferredSize(BUTTON_SIZE);
		modellerButton.setPreferredSize(BUTTON_SIZE);
		docButton.setPreferredSize(BUTTON_SIZE);
		
		logo.setPreferredSize(LOGO_DIMENSION);

		// Remplissage du panel de menu
		menuPanel.setLayout(new GridLayout(4, 1));
		menuPanel.add(menuLabel);
		menuPanel.add(modellerButton);
		menuPanel.add(explorerButton);
		menuPanel.add(docButton);
		
		// Remplissage du panel principal
		mainPanel.add(logo, BorderLayout.NORTH);
		mainPanel.add(menuPanel, BorderLayout.CENTER);

		// Ajout du panel principal
		add(mainPanel);

		// Couleur des panels
		menuPanel.setBackground(BACKGROUND_COLOR);
		mainPanel.setBackground(BACKGROUND_COLOR);
		logo.setBackground(BACKGROUND_COLOR);
		
		pack();
		
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setVisible(true);
	}

	/** Point d'entr�e de la classe */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				@SuppressWarnings("unused")
				Launcher l = new Launcher();
			}
			
		});
	}

}
