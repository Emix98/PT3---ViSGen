package fr.visgen.controller.modeller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Room;
import fr.visgen.view.modeller.ModellerFrame;

public class NewRoomController implements ActionListener {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerFrame frame;
	
	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application et de sa fen�tre
	 * @param modellerModel Mod�le de l'application
	 * @param frame Fen�tre de l'application
	 */
	public NewRoomController(ModellerModel model, ModellerFrame frame) 
	{
		modellerModel = model;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		modellerModel.addRoom();
		frame.getRoomModeLabel().setText("Vue de la pi�ce: " + modellerModel.getCurrentArea().getName());
		frame.repaint();
	}
}
