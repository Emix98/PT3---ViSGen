package fr.visgen.view.explorer;

import javax.swing.JPanel;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import fr.visgen.model.ExplorerModel;

public class Explorer3DView extends JPanel implements GLEventListener  {
	
	public Explorer3DView() {
		super();
	}
	
	 private ViewArea3DExplorer Zone3D = new ViewArea3DExplorer ();

	  /** Gestionnaire de visualisation de scene */
	  private ExplorerModel explorerModel = new ExplorerModel();


	  /** Appele par le drawable immediatement apres le contexte OpenGL2 est
	   * initialiser pour la premiere fois.
	   * Implementation � partir de GLEventListener.
	   * Utilise pour effectuer des initialisations OpenGL2 uniques.
	    * @param glDrawable GLAutoDrawable object.
	    */
	  public void init (GLAutoDrawable glDrawable)
	  {
	    // Parametre de lumiere
	    float _lightPos[] = {0.0f, 0.0f, 2.0f, 1.0f};
	    float _lightAmbient[] = {0.1f, 0.1f, 0.2f, 1.0f};
	    float _lightDiffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
	    float _lightSpecular[] = {0.6f, 0.6f, 0.6f, 1.0f};

	    final GL2 gl = glDrawable.getGL().getGL2 ();
	    gl.glShadeModel (GL2.GL_SMOOTH);                // Smooth Shading
	    gl.glClearColor (0.0f, 0.0f, 0.0f, 0.5f);      // Black Background
	    gl.glClearDepth (1.0f);                        // Depth Buffer Setup
	    gl.glEnable (GL2.GL_DEPTH_TEST);                // Enables Depth Testing
	    gl.glDepthFunc (GL2.GL_LEQUAL);                 // Type Of Depth Testing
	    gl.glEnable (GL2.GL_CULL_FACE);                 // Face culling

	    // Light model Permet d'afficher la lumi�re sur les diff�rents murs et donc d'avoir les couleurs
	    gl.glShadeModel (GL2.GL_SMOOTH);
	    gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_AMBIENT, _lightAmbient, 0);
	    gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_POSITION, _lightPos, 0);
	    gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_DIFFUSE, _lightDiffuse, 0);
	    gl.glLightfv (GL2.GL_LIGHT0, GL2.GL_SPECULAR, _lightSpecular, 0);
	    gl.glEnable (GL2.GL_LIGHT0);
	    gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_AMBIENT, _lightAmbient, 0);
	    gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_POSITION, _lightPos, 0);
	    gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_DIFFUSE, _lightDiffuse, 0);
	    gl.glLightfv (GL2.GL_LIGHT1, GL2.GL_SPECULAR, _lightSpecular, 0);
	    gl.glEnable (GL2.GL_LIGHT1);
	    gl.glEnable (GL2.GL_LIGHTING);

	    // Ajuste l'observateur � la scene pour l'afficher
	    explorerModel.setViewingVolume (Zone3D.defaultVisibleVolume ());
	    explorerModel.setAccessibleVolume (Zone3D.accessibleVolumepiece1 ());
	    explorerModel.setDefaultPosition (Zone3D.observerPosition ());

	    // Initialise la Zone3D
	    Zone3D.init (gl);
	  }

	  /** Appele par le drawable pour lancer le rendu OpenGL2 par le client.
	   * Implementation � partir de GLEventListener.
	   * Apres que tous les GLEventListeners ont ete avertis d'un evenement d'affichage,
	   * le drawable �changera ses tampons si necessaire.
	    * @param glDrawable GLAutoDrawable object.
	    */
	  public void display (GLAutoDrawable glDrawable)
	  {
	    final GL2 gl = glDrawable.getGL().getGL2 ();

	    gl.glClearColor (0.0f, 0.0f, 0.0f, 0.5f); // Black Background
	    gl.glClear (GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

	    // Defini le spectateur
	    explorerModel.apply (gl);

	    // Affiche la Zone3D
	    Zone3D.draw (gl);
	  }

	  /** Appele par le drawable au cours du premier affichage apres le composant
	   * a ete redimensionne.
	   * Impl�mentation � partir de GLEventListener.
	   * @param glDrawable Objet GLAutoDrawable.
	   * @param x X Coordonnee de la zone de la fenetre.
	   * @param y Y Coordonnee de la zone de la fenetre.
	   * @param largeur nouvelle largeur de la fenetre.
	   * @param hauteur nouvelle hauteur de la fenetre.
	    */
	  public void reshape (GLAutoDrawable glDrawable,
	                       int x, int y, int width, int height)
	  {
	    final GL2 gl = glDrawable.getGL().getGL2 ();

	    if (height <= 0) height = 1; // avoid a divide by zero error!
	    gl.glViewport (0, 0, width, height);
	    explorerModel.setProjection (gl, width, height);
	    gl.glLoadIdentity ();
	  }

	  /** Appele lorsque le mode d'affichage a ete modifie.
	    * Implementation � partir de GLEventListener.
	    * @param glDrawable GLAutoDrawable object.
	    */
	  public void dispose (GLAutoDrawable glDrawable)
	  {
	  }

	  /** Retourne la scene
	    */
	  public ViewArea3DExplorer scene()
	  {
	    return (Zone3D);
	  }

	  /** Retourne le controleur de visualisation de la scene.
	    */
	  public ExplorerModel Explor3D()
	  {
	    return (explorerModel);
	  }
	
	

}
