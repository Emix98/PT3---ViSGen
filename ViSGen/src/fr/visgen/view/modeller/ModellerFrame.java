package fr.visgen.view.modeller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.border.Border;

import fr.visgen.controller.modeller.DisplayWorkViewController;
import fr.visgen.controller.modeller.NewCorridorController;
import fr.visgen.controller.modeller.NewGameMapController;
import fr.visgen.controller.modeller.NewRoomController;
import fr.visgen.controller.modeller.NewSummitController;
import fr.visgen.controller.modeller.OpenGameMapController;
import fr.visgen.controller.modeller.OpenInExplorerController;
import fr.visgen.controller.modeller.RoomViewController;
import fr.visgen.controller.modeller.SaveGameMapController;
import fr.visgen.controller.modeller.TreeViewController;
import fr.visgen.model.ModellerModel;

/** Fen�tre du modeleur */
public class ModellerFrame extends JFrame {
	private static final long serialVersionUID = -7980079271866509905L;
	/** Mod�le, contenant entre autres la carte actuellement affich�e et l'outil/pi�ce actuellement s�lectionn�(e) */
	private ModellerModel modellerModel = new ModellerModel();


	// CONSTRUCTEURS
	/** Constructeur par d�faut */
	public ModellerFrame() {
		this("ViSGen Modeller");
	}

	/** Constructeur principal de la classe */
	public ModellerFrame(String name) {
		super(name);

		// Cr�ation des bords des panels
		Border raisedbevel = BorderFactory.createRaisedBevelBorder();
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		border = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);


		// Cr�ation de la barre de menu
		menuBar = new JMenuBar();

		// Cr�ation du menu Fichier
		fileMenu = new JMenu("Fichier");

		// Cr�ation du sous menu Nouveau...
		newMenu = new JMenu("Nouveau...");

		// Cr�ation du bouton permettant de cr�er une GameMap
		newGameMap = new JMenuItem("Carte");
		newMenu.add(newGameMap);

		// Ajout du menu "Nouveau" dans la barre de menu
		fileMenu.add(newMenu);

		// Cr�ation du bouton permettant d'ouvrir une GameMap
		openGameMap = new JMenuItem("Ouvrir...");
		fileMenu.add(openGameMap);

		// Cr�ation du bouton permettant de sauvegarder
		saveGameMap = new JMenuItem("Enregistrer");
		fileMenu.add(saveGameMap);

		// Cr�ation du bouton permettant d'ouvrir une GameMap �dit�e actuellement dans l'explorateur
		openInExplorer = new JMenuItem("Ouvrir dans l'explorateur");
		fileMenu.add(openInExplorer);
		menuBar.add(fileMenu); // Ajout du menu fichier dans la barre de menu

		// Cr�ation du menu Affichage
		displayMenu = new JMenu("Affichage");

		// Cr�ation du groupe de boutons permettant de changer la vue
		viewSelector = new ButtonGroup();

		// Cr�ation du bouton permettant de choisir la vue pi�ce
		displayRoomView = new JRadioButtonMenuItem("Vue pi�ce");
		viewSelector.add(displayRoomView);

		// Cr�ation du bouton permettant de choisir la vue graphe
		displayGraphView = new JRadioButtonMenuItem("Vue graphe");
		viewSelector.add(displayGraphView);

		displayRoomView.setSelected(true);

		//Ajout des deux boutons
		displayMenu.add(displayRoomView);
		displayMenu.add(displayGraphView);
		menuBar.add(displayMenu); // Ajout du menu affichage dans la barre de menu


		// Cr�ation du menu ajouter
		addMenu = new JMenu("Ajouter");

		// Cr�ation du bouton permettant de rajouter une pi�ce � la GameMap
		newRoom = new JMenuItem("Pi�ce");
		addMenu.add(newRoom);

		// Cr�ation du bouton permettant de rajouter un couloir � la GameMap
		newCorridor = new JMenuItem("Couloir");
		addMenu.add(newCorridor);

		addMenu.addSeparator();
		
		// Cr�ation du bouton permettant d'ajouter un point � la pi�ce actuelle
		newSummit = new JMenuItem("Point");
		addMenu.add(newSummit);

		// Cr�ation du bouton permettant d'ajouter une porte � la pi�ce actuelle
		newDoor = new JMenuItem("Porte");
		addMenu.add(newDoor);

		// Cr�ation du bouton permettant d'ajouter une fen�tre � la pi�ce actuelle
		newWindow = new JMenuItem("Fen�tre");
		addMenu.add(newWindow);

		menuBar.add(addMenu); // Ajout du menu ajouter � la barre de menu		


		// Ajout de la vue graphe, que l'on d�finit comme la vue actuelle et que l'on ajoute � la fen�tre actuelle
		graphView = new ModellerGraphView();
		graphView.setBorder(border);
		graphView.add(graphModeLabel);

		// Cr�ation de la vue de la pi�ce, que l'on garde pour l'instant en r�serve
		roomView = new ModellerRoomView(modellerModel);
		roomView.addMouseListener(new RoomViewController(modellerModel, roomView));
		roomView.addMouseListener(new NewSummitController(modellerModel));
		roomView.add(roomModeLabel);
		roomView.setBorder(border);

		// On ajoute la vue room par d�faut
		currentView = roomView;
		add(currentView, BorderLayout.CENTER);


		// Ajout de l'arborescence des �l�ments de la GameMap
		mapTree = new ModellerTreeView(modellerModel);
		mapTree.setBorder(border);
		mapTree.addMouseListener(new TreeViewController(modellerModel, mapTree));
		add(mapTree, BorderLayout.WEST);

		// Ajout des commandes les plus utilis�es
		utilitiesView = new ModellerUtilitiesView(modellerModel);
		utilitiesView.setBorder(border);
		add(utilitiesView, BorderLayout.EAST);


		// On abonne les vues au mod�le
		modellerModel.addObserver(roomView);
		modellerModel.addObserver(mapTree);
		modellerModel.addObserver(utilitiesView);


		// On abonne les boutons et menus aux contr�leurs
		newGameMap.addActionListener(new NewGameMapController(modellerModel));
		newRoom.addActionListener(new NewRoomController(modellerModel, this));
		newCorridor.addActionListener(new NewCorridorController(modellerModel, this));
		newSummit.addActionListener(new NewSummitController(modellerModel));

		openGameMap.addActionListener(new OpenGameMapController(modellerModel, this));
		saveGameMap.addActionListener(new SaveGameMapController(modellerModel, this));
		openInExplorer.addActionListener(new OpenInExplorerController(modellerModel, this));

		displayGraphView.addActionListener(new DisplayWorkViewController(this));
		displayRoomView.addActionListener(new DisplayWorkViewController(this));
		
		// On change la couleur de tous les panels � blanc
		mapTree.setBackground(BACKGROUND_COLOR);
		roomView.setBackground(BACKGROUND_COLOR);
		graphView.setBackground(BACKGROUND_COLOR);
		utilitiesView.setBackground(BACKGROUND_COLOR);
		setBackground(BACKGROUND_COLOR);


		// Finalisation de la fen�tre
		setSize(WINDOW_DIMENSION);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setJMenuBar(menuBar);

		setResizable(false);
		setVisible(true);
	}



	// GETTERS ET SETTERS

	/**
	 * Retourne la vue pi�ce
	 * @return Vue pi�ce
	 */
	public ModellerRoomView getRoomView() {
		return roomView;
	}

	/**
	 * Retourne la vue graphe
	 * @return Vue graphe
	 */
	public ModellerGraphView getGraphView() {
		return graphView;
	}

	/**
	 * Retourne la vue actuelle
	 * @return Vue actuelle
	 */
	public JPanel getCurrentView() {
		return currentView;
	}

	/**
	 * D�finit la vue actuelle
	 * @param currentView Vue � d�finir comme "vue actuelle"
	 */
	public void setCurrentView(JPanel currentView) {
		this.currentView = currentView;
	}

	/**
	 * Retourne le label indiquant la salle courante
	 * @return Label du mode pi�ce
	 */
	public JLabel getRoomModeLabel() {
		return roomModeLabel;
	}

	/**
	 * Retourne le label du mode graphe
	 * @return Label du mode graphe
	 */
	public JLabel getGraphModeLabel() {
		return graphModeLabel;
	}



	// ATTRIBUTS PRIVES (en bas afin de faciliter l'acc�s au code)

	/** Dimensions de base de la fen�tre */
	private static final Dimension WINDOW_DIMENSION = new Dimension(1280,720);

	//Corps principal de la fen�tre
	/** Panel contenant l'arborescence de la GameMap */
	private ModellerTreeView mapTree;
	/** Bloc contenant une vue de la pi�ce s�lectionn�e actuellement */
	private ModellerRoomView roomView;
	/** Bloc contenant une vue du graphe d'organisation des pi�ces */
	private ModellerGraphView graphView;
	/** Bloc contenant la vue actuelle (graphe ou pi�ce) */
	private JPanel currentView;
	/** Bloc contenant les commandes les plus souvent utilis�es et/ou les commandes relatives � l'objet actuellement s�lectionn� */
	private ModellerUtilitiesView utilitiesView;

	//Barre de menu
	/** Barre de menu en haut de l'�cran */
	private JMenuBar menuBar;
	//Menu fichier
	/** Menu fichier (nouveau, ouvrir, sauvegarder) */
	private JMenu fileMenu;
	/** Menu permettant de cr�er une nouvelle GameMap ou un nouvel objet pour la GameMap actuelle */
	private JMenu newMenu;
	/** Bouton permettant de cr�er une nouvelle GameMap */
	private JMenuItem newGameMap;
	/** Bouton permettant d'ouvrir une GameMap existante (c'est-�-dire un fichier .vsg) */
	private JMenuItem openGameMap;
	/** Bouton permettant de sauvegarder la GameMap actuelle */
	private JMenuItem saveGameMap;
	/** Bouton permettant d'ouvrir la GameMap actuelle dans l'explorateur */
	private JMenuItem openInExplorer;
	//Menu affichage
	/** Menu affichage (pour basculer de la vue graphe � la vue pi�ces, etc.) */
	private JMenu displayMenu;
	/** Bouton permettant de choisir d'afficher la vue du graphe de la GameMap actuelle */
	private JRadioButtonMenuItem displayGraphView;
	/** Bouton permettant de choisir d'afficher la vue de la pi�ce actuelle */
	private JRadioButtonMenuItem displayRoomView;
	/** Regroupement des boutons permettant de choisir la vue */
	private ButtonGroup viewSelector;
	// Menu ajouter
	/** Menu ajouter (pour ajouter des �l�ments � la GameMap */
	private JMenu addMenu;
	/** Bouton permettant d'ajouter une nouvelle pi�ce � la GameMap actuelle */
	private JMenuItem newRoom;
	/** Bouton permettant d'ajouter un nouveau couloir � la GameMap actuelle */
	private JMenuItem newCorridor;
	/** Bouton permettant d'ajouter un sommet � la pi�ce actuelle */
	private JMenuItem newSummit;
	/**Bouton permettant d'ajouter une nouvelle porte � la salle actuelle */
	private JMenuItem newDoor;
	/** Bouton permettant d'ajouter une nouvelle fen�tre � la salle actuelle */
	private JMenuItem newWindow;

	/** Label indiquant que l'on est en mode pi�ce */
	private JLabel roomModeLabel = new JLabel("Vue de la pi�ce: " + modellerModel.getCurrentArea().getName());
	/** Label indiquant que l'on est en mode graphe */
	private JLabel graphModeLabel = new JLabel("Vue du graphe");

	/** Bords des panels */
	private Border border;
	
	/** Couleur de fond */
	private final Color BACKGROUND_COLOR = Color.WHITE;
}