package fr.visgen.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/** Oblige une classe � d�clarer les m�thodes save() et load() */
public interface Loadable {
	/**
	 * Sauvegarde l'objet dans un fichier.
	 * @param data Flux de donn�es
	 * @throws IOException
	 */
	public void save(DataOutputStream data) throws IOException;
	/**
	 * Charge l'objet � partir d'un fichier.
	 * @param data Flux de donn�es.
	 * @throws IOException
	 */
	public void load(DataInputStream data) throws IOException;
}
