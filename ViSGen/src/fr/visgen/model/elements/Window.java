package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/** Repr�sente une fen�tre pouvant ouvrir vers un d�cor ou vers une autre pi�ce */
public class Window extends Opening {
	/** Hauteur par rapport au sol */
	private static final double HEIGHT_FROM_GROUND = 0.75;

	/**
	 * Construit une fen�tre � partir de ses coordon�es
	 * @param x Coordonn�e x
	 * @param y Coordon�e y
	 * @param i 
	 * @param object 
	 */
	public Window(int id, double x, double y, int linkedRoom) {
		super(id, x, y, linkedRoom);
		width = 1.5;
		height = 1.5;
	}
	
	/** Construit une fen�tre par d�faut */
	public Window() {
		this(0, 0, 0, MapArea.DEAD_END);
	}
}
