package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;

import fr.visgen.model.Loadable;
import fr.visgen.model.ModellerModel;

/** Repr�sente un point */
public class Point extends Selectable implements Loadable {
	/** Coordon�e x du points */
	private double x;
	/** Coordon�e y du point */
	private double y;
	
	/**
	 * Cr�e un point � l'aide des coordon�es x et y
	 * @param x Coordon�e x du point
	 * @param y Coordon�e y du point
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/** Construit un point par d�faut */
	public Point() {
		this(0, 0);
	}
	
	
	// METHODES
	/**
	 * Retourne true si le point est align� aux points p1 et p2 pass�s en param�tre
	 * @param p1 Premier point
	 * @param p2 Deuxi�me point
	 * @return True si le point est align� � p1 et p2
	 */
	public boolean isAligned(Point p1, Point p2) {
		// TODO Refaire cette m�thode parce que c'est mauvais
		
		// Coordon�es du premier point
		int x1 = (int) p1.getX() * ModellerModel.getScale() + ModellerModel.getMargin();
		int y1 = (int) p1.getY() * ModellerModel.getScale() + ModellerModel.getMargin();
		
		// Coordon�es du deuxi�me point
		int x2 = (int) p2.getX() * ModellerModel.getScale() + ModellerModel.getMargin();
		int y2 = (int) p2.getY() * ModellerModel.getScale() + ModellerModel.getMargin();
		
		if( (x1 * y2) - (x2 - y1) == 0 ) return true;
		return false;
	}

	
	
	// GETTERS ET SETTERS
	
	/**
	 * Retourne la coordon�e x du point
	 * @return Coordon�e x du point
	 */
	public double getX() {
		return x;
	}

	/**
	 * D�finit la coordon�e du x du point
	 * @param x Coordon�e x
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Retourne la coordon�e y du point
	 * @return Coordon�e y du point
	 */
	public double getY() {
		return y;
	}

	/**
	 * D�finit la coordon�e du y du point
	 * @param y Coordon�e y
	 */
	public void setY(double y) {
		this.y = y;
	}

	@Override
	public void save(DataOutputStream data) throws IOException {
		data.writeDouble(x);
		data.writeDouble(y);
	}

	@Override
	public void load(DataInputStream data) throws IOException {
		x = data.readDouble();
		y = data.readDouble();
	}}
