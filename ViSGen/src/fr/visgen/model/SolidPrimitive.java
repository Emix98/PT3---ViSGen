package fr.visgen.model;
import com.jogamp.opengl.GL2;


/** Classe abstraite pour les primitives solides */
public abstract class SolidPrimitive
{
  /** Positions du systeme de reference par rapport a la primitive solide
    */
  public final static int REF_CENTER = 0;
  public final static int REF_BASE = 1;

  protected float[] refMatrix = {1.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, 1.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 1.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 1.0f};

  /** Renvoie la reference du systeme.
    */
  protected abstract void setReference (int ref);

  /** Rend la primitive solide.
    * @param gl GL2 context. 
    */ 
  public abstract void draw (GL2 gl);
}
