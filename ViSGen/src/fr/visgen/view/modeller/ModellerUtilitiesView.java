package fr.visgen.view.modeller;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Point;
import fr.visgen.model.elements.Selectable;

public class ModellerUtilitiesView extends JPanel implements Observer {

	private static final long serialVersionUID = 1735836988906447045L;
	
	/** Mod�le, contenant entre autres l'objet actuellement s�lectionn� */
	private ModellerModel modellerModel;
	
	// DIMENSIONS
	/** Dimensions du menu */
	private final static Dimension MENU_DIMENSIONS = new Dimension(250, 100);
	/** Dimensions des textbox */
	private final static Dimension TEXTBOX_DIMENSIONS = new Dimension(200, 25);
	
	public ModellerUtilitiesView(ModellerModel m) {
		modellerModel = m;
		setPreferredSize(MENU_DIMENSIONS);
	}

	@Override
	public void update(Observable o, Object arg1) {
		Selectable s = modellerModel.getSelectedElement();
		removeAll();
		roomMenu();
		if(s instanceof Point) pointMenu();
		
	}
	
	/** G�n�re le menu par d�faut */
	private void roomMenu() {
		JTextField name = new JTextField(modellerModel.getCurrentArea().getName());
		name.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				modellerModel.getCurrentArea().setName(((JTextField) e.getSource()).getText());
				modellerModel.refresh();
			}
			
		});
		add(name);
	}
	
	/** G�n�re le menu de modification d'un point */
	private void pointMenu() {
		Point p = (Point) modellerModel.getSelectedElement();
		
		JTextField x = new JTextField(String.valueOf(p.getX()));
		JTextField y = new JTextField(String.valueOf(p.getY()));
		JButton validate = new JButton("Valider les modifications");
		
		x.setPreferredSize(TEXTBOX_DIMENSIONS);
		y.setPreferredSize(TEXTBOX_DIMENSIONS);
		
		x.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					System.out.println("test");
					p.setX(Double.valueOf(x.getText()));
					p.setY(Double.valueOf(y.getText()));
				}
			}
			
		});
		
		x.setEditable(true);
		
		validate.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent e) {
				p.setX(Double.valueOf(x.getText()));
				p.setY(Double.valueOf(y.getText()));
			}
			
		});

		add(new JLabel("Coordon�e x du point:"));
		add(x);
		add(new JLabel("Coordon�e y du point:"));
		add(y);
		add(validate);
		
		revalidate();
		repaint();
	}
}
