package fr.visgen.controller.modeller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.visgen.model.GameMap;
import fr.visgen.model.ModellerModel;
import fr.visgen.view.modeller.ModellerFrame;

public class OpenGameMapController implements ActionListener  {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;
	/** Fen�tre de l'application */
	private ModellerFrame frame;
	/** Fen�tre permettant de choisir l'emplacement du fichier � charger */
	private JFileChooser fileChooser;

	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application
	 * @param model Mod�le de l'application
	 * @param frame Fen�tre de l'application
	 */
	public OpenGameMapController(ModellerModel model, ModellerFrame frame) 
	{
		modellerModel = model;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (fileChooser == null) fileChooser = new JFileChooser(new File(System.getProperty("user.dir")));
		FileNameExtensionFilter filter = new FileNameExtensionFilter ("Projets ViSGen (.vsg)", "vsg");
		fileChooser.setFileFilter(filter);
		int returnVal = fileChooser.showOpenDialog(frame.getParent());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File savedFile = fileChooser.getSelectedFile();
			modellerModel.load(savedFile);
		}
	}
	/**public void actionPerformed(ActionEvent act) {

		try {
			if (fileChooser == null) fileChooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter ("Projets ViSGen (.vsg)", "vsg");
			fileChooser.setFileFilter(filter);
			int returnVal = fileChooser.showOpenDialog(frame.getParent());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				System.out.println("Fichier s�lectionn� : "+fileChooser.getSelectedFile()+". Bonne journ�e !");
			}
		} catch (Exception e) {

		}

	}*/
}
