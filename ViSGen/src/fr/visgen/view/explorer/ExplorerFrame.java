package fr.visgen.view.explorer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import com.jogamp.opengl.awt.GLCanvas;

import fr.visgen.controller.explorer.ExplorerMovement;
import fr.visgen.controller.modeller.OpenInExplorerController;
import fr.visgen.model.ExplorerModel;
import fr.visgen.model.GameMap;


public class ExplorerFrame extends JFrame{
	private static final long serialVersionUID = -7980079271866509905L;
	
	/** Dimensions de base de la fen�tre */
	private static final Dimension WINDOW_DIMENSION = new Dimension(1280,720);
	
	/** Mod�le de l'application */
	private ExplorerModel explorerModel;
	
	/** Vue 3D de la GameMap */
	private Explorer3DView gameMapView;
	
	/** Barre de menu en haut de l'�cran */
	private JMenuBar menuBar;
	
	/** Menu fichier (ouvrir une GameMap) */
	private JMenu fileMenu;
	
	/**Si on ne pr�cise pas le nom de la fen�tre ni la GameMap, on en utilise un par d�faut et on n'affiche pas de GameMap*/
	public ExplorerFrame() {
		this("ViSGen Explorer",null);
		
	}

	/**Construit la fen�tre de l'explorateur en lui donnant comme nom une cha�ne pass�e en param�tre, mais sans passer de GameMap*/
	public ExplorerFrame(String name) {
		this(name,null);
	}
	
	
	
	/**Construit la fen�tre de l'explorateur en lui donnant un nom et une GameMap pr�d�finie*/
	public ExplorerFrame(String name, GameMap gameMap) {
		super(name);
		
		GLCanvas canvas = new GLCanvas ();
		
		gameMapView = new Explorer3DView();
		canvas.addGLEventListener (gameMapView);
		
		ExplorerMovement myController = new ExplorerMovement (canvas, gameMapView);
		canvas.addKeyListener(myController);
		canvas.addMouseListener(myController);
		canvas.addMouseMotionListener (myController);
		    
		add(canvas,BorderLayout.CENTER);
		
		menuBar = new JMenuBar();
		
		fileMenu = new JMenu("Fichier");
		menuBar.add(fileMenu);
		
		setJMenuBar(menuBar);

		//Finalisation de la fen�tre
		setSize(WINDOW_DIMENSION);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		
		canvas.requestFocus ();
	}

}