package fr.visgen.controller.modeller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import fr.visgen.model.ModellerModel;
import fr.visgen.model.elements.Point;
import fr.visgen.view.modeller.ModellerRoomView;

public class NewSummitController implements ActionListener, MouseListener {
	/** Mod�le du modeleur */
	private ModellerModel modellerModel;
	
	/**
	 * Construit le contr�leur � l'aide du mod�le de l'application et de sa fen�tre
	 * @param modellerModel Mod�le de l'application
	 * @param frame Fen�tre de l'application
	 */
	public NewSummitController(ModellerModel model) 
	{
		modellerModel = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		modellerModel.setInPlacement(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent mE) {
		if(mE.getSource() instanceof ModellerRoomView && modellerModel.isInPlacement() == true) {
			modellerModel.addSummit(new Point( (mE.getX() - ModellerModel.getMargin()) / ModellerModel.getScale(), ( mE.getY() - ModellerModel.getMargin() ) / ModellerModel.getScale()));
			modellerModel.setInPlacement(false);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
