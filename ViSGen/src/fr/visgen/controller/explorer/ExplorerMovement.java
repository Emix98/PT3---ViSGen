package fr.visgen.controller.explorer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import fr.visgen.model.ExplorerModel;
import fr.visgen.view.explorer.Explorer3DView;

public class ExplorerMovement implements KeyListener, MouseListener, MouseMotionListener{
	
	/** Controle la zone d'affichage. */
	  private GLCanvas canvas;
	  /** Controle le contexte OpenGL  */
	  private Explorer3DView explo3D;
	  /** Tache de fond. */
	  private FPSAnimator animator = null;
	  
		private boolean SourisEntree;
		private boolean SourisDeplacee;
		private int SourisX, SourisY;

	public ExplorerMovement(GLCanvas canvas, Explorer3DView explo3D)
    {
		this.explo3D = explo3D;
		this.canvas = canvas;
		animator = new FPSAnimator (canvas, 60, true);
		animator.start ();
    }

		  /** Invoque quand une touche est presse.
		    * Implementation from KeyListener.
		    * @param e detected key event.
		    */
		  public void keyPressed (KeyEvent e)
		  {
		    processKeyEvent (e, true);
		  }

		  /** Invoque quand une touche est relache.
		    * Implementation from KeyListener.
		    * @param e detected key event.
		    */
		  public void keyReleased (KeyEvent e)
		  {
		    processKeyEvent (e, false);
		  }

		  /** Invoque quand une touche est relache ou presse.
		    * Local implementation from KeyListener.
		    * @param e detected key event.
		    * @param pressed pressed or released key status.
		    */
		  private void processKeyEvent (KeyEvent e, boolean pressed)
		  {
		    switch (e.getKeyCode ())
		    {
		      case KeyEvent.VK_E :
		      case KeyEvent.VK_ESCAPE :
		        if (! pressed) System.exit (0);
		        break;

		      case KeyEvent.VK_HOME :
		        if (! pressed) explo3D.Explor3D().resetProjection ();
		        break;

		      case KeyEvent.VK_UP :
		        if (pressed)
		          if (e.isControlDown ())
		        	  explo3D.Explor3D().move (ExplorerModel.TURN_UP);
		          else explo3D.Explor3D().move (ExplorerModel.GO_FRONTWARDS);
		        else
		        {
		        	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
		        	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
		        }
		        break;

		      case KeyEvent.VK_DOWN :
		        if (pressed)
		          if (e.isControlDown ())
		        	  explo3D.Explor3D().move (ExplorerModel.TURN_DOWN);
		          else explo3D.Explor3D().move (ExplorerModel.GO_BACKWARDS);
		        else
		        {
		        	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
		        	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
		        }
		        break;

		      case KeyEvent.VK_LEFT :
		        if (pressed)
		          if (e.isControlDown ())
		        	  explo3D.Explor3D().move (ExplorerModel.TURN_LEFT);
		          else explo3D.Explor3D().move (ExplorerModel.GO_LEFTWARDS);
		        else
		        {
		        	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
		        	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
		        }
		        break;

		      case KeyEvent.VK_RIGHT :
		        if (pressed)
		          if (e.isControlDown ())
		        	  explo3D.Explor3D().move (ExplorerModel.TURN_RIGHT);
		          else explo3D.Explor3D().move (ExplorerModel.GO_RIGHTWARDS);
		        else
		        {
		        	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
		        	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
		        }
		        break;

		      case KeyEvent.VK_PAGE_UP :
		        if (pressed) explo3D.Explor3D().move (ExplorerModel.GO_UPWARDS);
		        else explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_GO);
		        break;

		      case KeyEvent.VK_PAGE_DOWN :
		        if (pressed) explo3D.Explor3D().move (ExplorerModel.GO_DOWNWARDS);
		        else explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_GO);
		        break;
		        
		        
		        
		      // Controle de la camera par les touches  
		      case KeyEvent.VK_D :
		    	  if (pressed)
		              if (e.isControlDown ())
		            	  explo3D.Explor3D().move (ExplorerModel.GO_RIGHTWARDS);
		              else explo3D.Explor3D().move (ExplorerModel.TURN_RIGHT);
		            else
		            {
		            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
		            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
		            }
		    	  break;
		    	  
		      case KeyEvent.VK_Q :
		    	  if (pressed)
		              if (e.isControlDown ())
		            	  explo3D.Explor3D().move (ExplorerModel.GO_RIGHTWARDS);
		              else explo3D.Explor3D().move (ExplorerModel.TURN_LEFT);
		            else
		            {
		            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
		            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
		            }
		    	  break;
		    	  
		      case KeyEvent.VK_Z :
		    	  if (pressed)
		              if (e.isControlDown ())
		            	  explo3D.Explor3D().move (ExplorerModel.GO_FRONTWARDS);
		              else explo3D.Explor3D().move (ExplorerModel.TURN_UP);
		            else
		            {
		            	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
		            	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
		            }
		            break;
		            
		      case KeyEvent.VK_S :
		    	  if (pressed)
		              if (e.isControlDown ())
		            	  explo3D.Explor3D().move (ExplorerModel.GO_BACKWARDS);
		              else explo3D.Explor3D().move (ExplorerModel.TURN_DOWN);
		            else
		            {
		            	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
		            	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
		            }
		      break;
		    }
		  }

		  /** Invoque quand une cle est tape.
		    * Implementation from KeyListener.
		    * @param e detected key event.
		    */
		  public void keyTyped (KeyEvent e)
		  {
		    switch (e.getKeyChar ())
		    {
		      case '+' :
		    	  explo3D.Explor3D().zoomIn ();
		        break;
		      case '-' :
		    	  explo3D.Explor3D().zoomOut ();
		        break;
		      case 'p' :
		    explo3D.Explor3D().toggleProjection ();
			canvas.display ();
			break;
		    }
		  }

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}	
		
		public boolean mouseEnter(MouseEvent e, int x, int y)
		{
			SourisEntree = true;
			return true;
		}
		
		/** Invoque quand une souris sort de la zone d'affichage
		 * @param e
		 * @return
		 */
		public boolean mouseExit(MouseEvent e)
		{
			SourisEntree = false;
			return true;
		}	
		
		/** Invoque quand une souris est deplace dans la zone d'affichage
		 * 
		 */
		
		public void mouseMoved(MouseEvent e)
		{
			/**
			SourisDeplacee = true;
			SourisX = e.getX();
			SourisY = e.getY();
			
			if (SourisDeplacee && SourisY>720/2-100)
				if (e.isControlDown ())
	            	  explo3D.Explor3D().move (ExplorerModel.GO_BACKWARDS);
	              else explo3D.Explor3D().move (ExplorerModel.TURN_DOWN);
	            else
	            {
	            	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
	            	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
	            }
			
			if (SourisDeplacee && SourisX<1280/2+100)
				if (e.isControlDown ())
	            	  explo3D.Explor3D().move (ExplorerModel.GO_RIGHTWARDS);
	              else explo3D.Explor3D().move (ExplorerModel.TURN_LEFT);
	            else
	            {
	            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
	            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
	            }
			
			if (SourisDeplacee && SourisY<720/2-100)
				if (e.isControlDown ())
	            	  explo3D.Explor3D().move (ExplorerModel.GO_FRONTWARDS);
	              else explo3D.Explor3D().move (ExplorerModel.TURN_UP);
	            else
	            {
	            	explo3D.Explor3D().move (ExplorerModel.NO_FRONT_BACK_GO);
	            	explo3D.Explor3D().move (ExplorerModel.NO_UP_DOWN_TURN);
	            }
	        
			if (SourisDeplacee && SourisX>1280/2+100)
				if (e.isControlDown ())
	            	  explo3D.Explor3D().move (ExplorerModel.GO_RIGHTWARDS);
	              else explo3D.Explor3D().move (ExplorerModel.TURN_RIGHT);
	            else
	            {
	            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_GO);
	            	explo3D.Explor3D().move (ExplorerModel.NO_RIGHT_LEFT_TURN);
	            }

			
			System.out.println("CoordX"+SourisX +"CoordY"+ SourisY);*/
		}
		

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		

}
