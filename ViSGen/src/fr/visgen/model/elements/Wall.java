package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import fr.visgen.model.Loadable;

/** Repr�sente un mur, lui m�me repr�sent� par deux points */
public class Wall extends Selectable implements Loadable {
	/** Premier point du mur */
	private Point p1;
	/** Deuxi�me point du mur */
	private Point p2;
	/** Valeurs a b et c (stock�es respectivement aux indices 0, 1 et 2 composant l'�quation cart�sienne de la droite du mur */
	private double[] equation = new double[2];
	
	/** Construit un mur � l'aide de deux points */
	public Wall(Point p1, Point p2) {
		super();
		this.p1 = p1;
		this.p2 = p2;
		this.equation = initEquation();
	}
	
	/**
	 * Initialise l'�quation de droite du mur
	 * @return a, b et c (de l'�quation ax+by+c=0)
	 */
	public double[] initEquation() {
		
		double slope = ( p2.getY() - p1.getY() ) / ( p2.getX() - p1.getX() );
		double c = -(slope)*p1.getX() + p1.getY(); //y=ax+c <=> c=-ax+y
		double[] equation = { slope, -1, c }; //a, b et c. b = -1 car ax+b=y <=> ax-y+c=0
		return equation;
		
	}
	
	/**
	 * Indique la position relative des deux points constituants le mur.
	 * Une valeur positive dans l'une des cases indique que P2 est � droite/en haut de P1
	 * @return relPos : Tableau contenant les positions relatives de P1 et P2 (la premi�re case pour l'abscisse, la deuxi�me pour l'ordonn�e)
	 */
	public double[] getRelPos() {
		double[] relPos = { p2.getX() - p1.getX(), p2.getY() - p1.getY() };
		return relPos;
		
	}
	
	/**
	 * Retourne les valeurs composant l'�quation cart�sienne du mud
	 * @return Tableau comprenant les trois valeurs a b et c de l'�quation de droite du mur
	 */
	public double[] getEquation() {
		
		return equation;
		
	}

	/**
	 * Retourne le premier point du mur
	 * @return Premier point du mur
	 */
	public Point getP1() {
		return p1;
	}

	/**
	 * D�finit le premier point du mur
	 * @param p1 Premier point du mur
	 */
	public void setP1(Point p1) {
		this.p1 = p1;
	}

	/**
	 * Retourne le deuxi�me point du mur
	 * @return Deuxi�me point du mur
	 */
	public Point getP2() {
		return p2;
	}

	/**
	 * D�finit le deuxi�me point du mur
	 * @param p2 Deuxi�me point du mur
	 */
	public void setP2(Point p2) {
		this.p2 = p2;
	}

	@Override
	public void save(DataOutputStream data) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void load(DataInputStream data) throws IOException {
		// TODO Auto-generated method stub
		
	}
}
