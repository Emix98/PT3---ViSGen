package reshape;

public class reshape {

	void reshape(int w, int h)
	{
		glViewport(0,0,(GLsizei) w, (GLsizei) h); /** Attribue au rectangle la taille de la nouvelle fen�tre */
		/** Les deux premi�res valeurs place l'angle inf�rieurs � gauche et les deux derniers � l'angle sup�rieur droit */
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0.0,(GLdouble) w, 0.0, (GLdouble) h);/** Permettra de cr�er des carr�es dans le rectangle cr�e */ 
	}
	
	
}
