package fr.visgen.model.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Repr�sente un couloir */
public class Corridor extends MapArea {

	/** Construit un couloir par d�faut */
	public Corridor(int id) {
		super(id);

		// Nom par d�faut
		name = "Couloir sans nom ";

		// G�n�re le couloir par d�faut
		// Cr�e les points
		Point[] pointList = new Point[] {
				new Point(0, 7),
				new Point(16, 7),
		};

		// Cr�e la liste des sommets de la pi�ce
		summits = new ArrayList<Point>();

		// Ajoute les points dans la liste
		for(Point p : pointList) {
			summits.add(p);
		}

	}

	/** Construit un couloir � partir de son id, d'une liste de points et de deux portes */
	public Corridor(int id, List<Point> shape, List<Opening> openings) {
		super(id, shape, openings);
		// Nom par d�faut
		name = "Couloir sans nom ";
	}
	
	/**
	 * Ajoute un sommet � la suite du couloir
	 * @param summit Sommet � ajouter
	 */
	public void addSummit(Point summit) {
		summits.add(summit);
	}
	
	/**
	 * Permet de d�finir le nom du couloir
	 * @param name Nom du couloir
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public List<Wall> getWalls() { return null; }
}
